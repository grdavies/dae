Param(
  [Parameter(Mandatory=$True,Position=1)]
  [string]$datasource_name,
  [Parameter(Mandatory=$True,Position=2)]
  [string]$build_cmds,
  [Parameter(Mandatory=$True,Position=3)]
  [string]$build_docs,
  [Parameter(Mandatory=$True,Position=4)]
  [string]$build_id
  
)

# Create the Name of rhis script
$this_script = ($MyInvocation.MyCommand).Name

# Define Global Objects for As Built
$as_built_build_id						= New-Object System.Collections.ArrayList
$as_built_customer						= New-Object System.Collections.ArrayList
$as_built_networks						= New-Object System.Collections.ArrayList

# Define Global Data Values from the Globals Spreadsheet
#region Get global data
$global_customer | %{			
	$customer_name			= $_.customer_name
	$contact_name			= $_.contact_name
	$contact_email			= $_.contact_email
	$contact_phone			= $_.contact_phone
	}
$global_location | %{
	$customer_address		= $_.customer_address
	$system_install_address	= $_.system_install_address
	$rma_address			= $_.rma_address
	$rma_attn_to_name		= $_.rma_attn_to_name
	}
$global_datacenter | %{
	$datacenter_name		= $_.datacenter_name
	$domain_name			= $_.domain_name
	$dns_server_ips_str		= $_.dns_server_ips
	$ntp_server_names_str	= $_.ntp_server_names
	$smtp_server_name		= $_.smtp_server_name
	$snmp_server_name		= $_.snmp_server_name
	$snmp_community_string	= $_.snmp_community_string
	$timezone				= $_.timezone
	$default_admin_user		= $_.default_admin_user
	$default_admin_pass		= $_.default_admin_pass
	
	if ($dns_server_ips_str -match ",")
		{
		$dns_server_ips		= $dns_server_ips_str.Split(",")
		}
	else
		{
		$dns_server_ips		= @($dns_server_ips_str)
		}
	if ($ntp_server_names_str -match ",")
		{
		$ntp_server_names	= $ntp_server_names_str.Split(",")
		}
	else
		{
		$ntp_server_names	= @($ntp_server_names_str)
		}
	}
	
	if (!$default_admin_pass) {
		$default_admin_pass = "Datalink1"
	}
	
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Contact Name"
$as_built_customer_obj.customer_value	= $contact_name
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Contact Email"
$as_built_customer_obj.customer_value	= $contact_email
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Contact Phone"
$as_built_customer_obj.customer_value	= $contact_phone
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Customer Address"
$as_built_customer_obj.customer_value	= $customer_address
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "System Install Address"
$as_built_customer_obj.customer_value	= $system_install_address	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Rma Address"
$as_built_customer_obj.customer_value	= $rma_address	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Rma Attn To Name"
$as_built_customer_obj.customer_value	= $rma_attn_to_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Domain Name"
$as_built_customer_obj.customer_value	= $domain_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Dns Server Ips"
$as_built_customer_obj.customer_value	= $dns_server_ips
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Ntp Server Names"
$as_built_customer_obj.customer_value	= $ntp_server_names	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Smtp Server Name"
$as_built_customer_obj.customer_value	= $smtp_server_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Snmp Server Name"
$as_built_customer_obj.customer_value	= $snmp_server_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Snmp Community String"
$as_built_customer_obj.customer_value	= $snmp_community_string
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Timezone"
$as_built_customer_obj.customer_value	= $timezone	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Default Admin User"
$as_built_customer_obj.customer_value	= $default_admin_user	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Default Admin Pass"
$as_built_customer_obj.customer_value	= $default_admin_pass	
[Void]$as_built_customer.Add($as_built_customer_obj)

#endregion

### vspherebuilder assumption variables
	$esxi_mgmt_build_vmnic = "vmnic0"
	$ks_boot_proto = "static"
	$net_tcpipheapsize = 30
	$net_tcpipheapmax = 120
	$nfs_heartbeatmaxfailures = 10
	$nfs_heartbeatfrequency = 20
	$nfs_heartbeattimeout = 10
	$nfs_maxvolumes = 128
####################### Script Functional Start

###### Set Build_ID Data
	$vswitch_data 			= $esxi_data.Get_Item("esxi_cluster_vSwitches")
	$advanced_settings 		= $esxi_data.Get_Item("esxi_adv")
	$esxi_hosts		   		= $esxi_data.Get_Item("esxi_hosts")
	$vm_portgroup_data 		= $esxi_data.Get_Item("esxi_vmportgroups")
	$vmk_port_data			= $esxi_data.Get_Item("esxi_vmkports")


###### Set Data-Center Level Variables
	$iso_menu_filename = $customer_name + "_" + $build_id + "_" + "isolinux.cfg"
	$dc_default_vmnet = $advanced_settings.esxi_default_vm_net
	$dc_esxi_enable_ssh = $advanced_settings.esxi_enable_ssh
	$dc_esxi_enable_shell = $advanced_settings.esxi_enable_shell
	$dc_esxi_ssh_suppress_warn = $advanced_settings.esxi_ssh_suppress_warn
	$dc_esxi_shell_suppress_warn = $advanced_settings.esxi_shell_suppress_warn
	$dc_local_vmfs_prefix = $advanced_settings.esxi_local_vmfs_prefix
	$search_domains = $advanced_settings.esxi_search_domains

###### Grab vSwitch Data

	$vswitch_total = $vswitch_data.Count
	if ($vswitch_total -gt 0) {
		echo "$vSwitch_total vSwitch(es) in Build"
	} else {
		echo "0 vSwitch(es) in Build"
	}
###### Grab vmk_Portgroup Data
	$vmk_ports_mgmt = $vmk_port_data | where {$_.vmk_role -eq "management"}
	$vmk_ports_vmo = $vmk_port_data | where {$_.vmk_role -match "vmo"}
	$vmk_ports_nfs = $vmk_port_data | where {$_.vmk_role -eq "nfs"}
	$vmk_ports_iscsi = $vmk_port_data | where {$_.vmk_role -match "iscsi"}
	#Added by RD 2-13-2015
	$vmk_ports_vsan = $vmk_port_data | where {$_.vmk_role -match "vsan"}
	#
	$vmk_ports_vmotion_total = $vmk_ports_vmo.Count
	if($vmk_ports_vmotion_total -eq $null){$vmk_ports_vmotion_total = 1}
	$vmk_ports_mgmt_total = $vmk_ports_mgmt.Count
	if($vmk_ports_mgmt_total -eq $null){$vmk_ports_mgmt_total = 1}
	$vmk_ports_nfs_total = $vmk_ports_nfs.Count
	if($vmk_ports_nfs_total -eq $null){$vmk_ports_nfs_total = 1}
	$vmk_ports_iscsi_total = $vmk_ports_iscsi.Count
	#if($vmk_ports_iscsi_total -eq $null){$vmk_ports_iscsi_total = 1}
	if($vmk_ports_vmotion_total -gt 1){"MultiNic_vMotion with $vmk_ports_vmotion_total ports defined."}
	Elseif($vmk_ports_vmotion_total -eq 1){"SingleNic_vMotion with $vmk_ports_vmotion_total ports defined."}
	if($vmk_ports_iscsi_total -eq 0){"iSCSI interfaces not found"}
	Elseif($vmk_ports_iscsi -ne 2){"Expected two interfaces for iSCSI, but the number doesn't equal 2"}
	
	# Identify which switch has the mgmt vSwitch role by looking at VMK ports configured for mgmt
	$vswitch_role_management = $vmk_port_data | where {$_.vmk_role -eq "management"}
	if($vswitch_role_management -is [System.Array])
		{
			$vswitch_role_management = ($vswitch_role_management | sort -Property vswitch -Unique).vswitch
			if($vswitch_role_management -is [System.Array])
				{echo "More than one vSwitch defined for Management. Script not built for that yet."}
		}
	Elseif($vswitch_role_management -isnot [System.Array])
		{
			$vswitch_role_management = $vswitch_role_management.vswitch
		}
	
	# Identify which switch has the vMotion vSwitch role by looking at VMK ports configured for vMotion
	$vswitch_role_vmotion = $vmk_port_data | where {$_.vmk_role -match "vmotion"}
	if($vswitch_role_vmotion -is [System.Array])
		{
			$vswitch_role_vmotion = ($vswitch_role_vmotion | sort -Property vswitch -Unique).vswitch
			if($vswitch_role_vmotion -is [System.Array])
				{echo "More than one vSwitch defined for vMotion. Script not built for that yet."}
		}
	Elseif($vswitch_role_vmotion -isnot [System.Array])
		{
			$vswitch_role_vmotion = $vswitch_role_vmotion.vswitch
		}
	
	# Identify which switch has the NFS vSwitch role by looking at VMK ports configured for nfs
	$vswitch_role_nfs = $vmk_port_data | where {$_.vmk_role -eq "nfs"}
	if($vswitch_role_nfs -is [System.Array])
		{
			$vswitch_role_nfs = ($vswitch_role_nfs | sort -Property vswitch -Unique).vswitch
			if($vswitch_role_nfs -is [System.Array])
			{echo "More than one vSwitch defined for NFS. Script not built for that yet."}
		}
	Elseif($vswitch_role_nfs -isnot [System.Array])
		{
			$vswitch_role_nfs = $vswitch_role_nfs.vswitch
		}
	
	# Identify which switch has the iSCSI vSwitch role by looking at VMK ports configured for iSCSI
	$vswitch_role_iscsi = $vmk_port_data | where {$_.vmk_role -match "iscsi"}
	if($vswitch_role_iscsi -is [System.Array])
		{
			$vswitch_role_iscsi = ($vswitch_role_iscsi | sort -Property vswitch -Unique).vswitch
			if($vswitch_role_iscsi -is [System.Array])
			{echo "More than one vSwitch defined for iSCSI. Script not built for that yet."}
		}
	Elseif($vswitch_role_iscsi -isnot [System.Array])
	{
		$vswitch_role_iscsi = $vswitch_role_iscsi.vswitch
	}
	
	
######### Define Global Sections for the KS File
	$iso_menu							= New-Object System.Collections.ArrayList
	$ks_title							= New-Object System.Collections.ArrayList
	$ks_header							= New-Object System.Collections.ArrayList
	$ks_disk_partitioning				= New-Object System.Collections.ArrayList
	$ks_install_media					= New-Object System.Collections.ArrayList
	$ks_root_password					= New-Object System.Collections.ArrayList
	$ks_default_mgmt_int				= New-Object System.Collections.ArrayList
	$ks_default_mgmt_int_data			= New-Object System.Collections.ArrayList
	$ks_reboot_after_install_flag		= New-Object System.Collections.ArrayList
	$ks_script_pre						= New-Object System.Collections.ArrayList
	$ks_script_post						= New-Object System.Collections.ArrayList
	$ks_script_1st_reboot				= New-Object System.Collections.ArrayList
	$ks_vswitch_config					= New-Object System.Collections.ArrayList
	$ks_portgroup_config				= New-Object System.Collections.ArrayList
	#Added by RD 2-13-2015
	$ks_vsan_config						= New-Object System.Collections.ArrayList
	$ks_disable_ipv6					= New-Object System.Collections.ArrayList
	#
	$ks_rename_ds						= New-Object System.Collections.ArrayList
	$ks_remote_ssh_shell				= New-Object System.Collections.ArrayList
	$ks_add_mgmt_vmk0					= New-Object System.Collections.ArrayList
	$ks_advanced_settings				= New-Object System.Collections.ArrayList
	$ks_ntp								= New-Object System.Collections.ArrayList
	$ks_firewall						= New-Object System.Collections.ArrayList
	$ks_search_domains					= New-Object System.Collections.ArrayList
	$ks_wrap_up							= New-Object System.Collections.ArrayList
	$ks_all_cmds						= @()

########### Sets the values for the KS header before looping into the hosts
	[Void]$ks_header.Add("$section_break")
	[Void]$ks_header.Add("$section_break")
	[Void]$ks_header.Add("# Written By: $script_user")
	[Void]$ks_header.Add("# Date: $date_time")
	[Void]$ks_header.Add("# Produced for $customer_name")
	[Void]$ks_header.Add("# ClusterName: $global_esxi_cluster_name")
	[Void]$ks_header.Add("# HostType: $global_esxi_host_model")
	[Void]$ks_header.Add("# Datalink Automation Engineering")
	[Void]$ks_header.Add("$section_break")
	[Void]$ks_header.Add("$section_break")
	[Void]$ks_header.Add("# Accept License agreement")
	[Void]$ks_header.Add("$section_break")
	[Void]$ks_header.Add("vmaccepteula")
	[Void]$ks_header.Add("")

############### Sets the value of the Disk Partitioning section
	[Void]$ks_disk_partitioning.Add("$section_break")
	[Void]$ks_disk_partitioning.Add("# Disk Partitioning")

	[Void]$ks_disk_partitioning.Add("$section_break")
	if($advanced_settings.esxi_boot_device -eq "usb")
		{
			[Void]$ks_disk_partitioning.Add("# Clear all partitions in first detected USB device")
			[Void]$ks_disk_partitioning.Add("clearpart --firstdisk=usb")
		}
	Elseif($advanced_settings.esxi_boot_device -eq "disk")
		{
			[Void]$ks_disk_partitioning.Add("# Clear all partitions in first detected disk and overwrite any VMFS")
			[Void]$ks_disk_partitioning.Add("# partitions on the specified drives.")
			[Void]$ks_disk_partitioning.Add("clearpart --firstdisk --overwritevmfs")
		}
	Elseif($advanced_settings.esxi_boot_device -eq "san")
		{
			echo "Entry in global presite esxi_adv tab for esxi_boot_device type not currently handled";exit
		}
	Else
		{
			echo "Entry in global presite esxi_adv tab for esxi_boot_device not currently listed";exit
		}
	[Void]$ks_disk_partitioning.Add("")
	
######## Set the installation media location
	[Void]$ks_install_media.Add("$section_break")
	[Void]$ks_install_media.Add("# Installation location")
	[Void]$ks_install_media.Add("$section_break")
	
	if($advanced_settings.esxi_boot_device -eq "usb")
		{
			[Void]$ks_install_media.Add("# Fresh installation on first usb device")
			[Void]$ks_install_media.Add("install --firstdisk=usb")
		}
	Elseif($advanced_settings.esxi_boot_device -eq "disk")
		{
			[Void]$ks_install_media.Add("# Fresh installation on first disk and overwrite an existing VMFS datastore")
			[Void]$ks_install_media.Add("install --firstdisk --overwritevmfs")
		}
	Elseif($advanced_settings.esxi_boot_device -eq "san")
		{
			echo "Entry in global presite esxi_adv tab for esxi_boot_device type not currently handled";exit
		}
	Else
		{
			echo "Entry in global presite esxi_adv tab for esxi_boot_device not currently listed";exit
		}
	[Void]$ks_install_media.Add("")

######## Set the root password 
	[Void]$ks_root_password.Add("$section_break")
	[Void]$ks_root_password.Add("# Root password and Authentication format")
	[Void]$ks_root_password.Add("# Default is shadow password enabled, MD5-based passwords enabled")
	#[Void]$ks_root_password.Add("# Encrypted Root Password in MD5 format")
	#[Void]$ks_root_password.Add("# root password in MD5 format")
	[Void]$ks_root_password.Add("$section_break")
	[Void]$ks_root_password.Add("rootpw $default_admin_pass")
	#$rootPassword = Get-StringHash "$default_admin_pass" "MD5"
	#[Void]$ks_root_password.Add("rootpw --iscrypted $rootPassword")
	[Void]$ks_root_password.Add("")

######## Set the management interface header
	[Void]$ks_default_mgmt_int.Add("$section_break")
	[Void]$ks_default_mgmt_int.Add("# Set default Management Interface")
	[Void]$ks_default_mgmt_int.Add('# addvmportgroup set to "0" to disable the creation of default guest VM Network"')
	[Void]$ks_default_mgmt_int.Add("$section_break")

######## Set the reboot to not eject
	[Void]$ks_reboot_after_install_flag.Add("$section_break")
	[Void]$ks_reboot_after_install_flag.Add("# Reboot after installation")
	[Void]$ks_reboot_after_install_flag.Add("$section_break")
	[Void]$ks_reboot_after_install_flag.Add("reboot --noeject")
	[Void]$ks_reboot_after_install_flag.Add("")
	
######## Set the prescript section
	[Void]$ks_script_pre.Add("$section_break")
	[Void]$ks_script_pre.Add("# Specifies script to run before the kickstart configuration is evaluated")
	[Void]$ks_script_pre.Add("$section_break")
	[Void]$ks_script_pre.Add("%pre --interpreter=busybox")
	[Void]$ks_script_pre.Add("")
	
######## Set the Postscript
	[Void]$ks_script_post.Add("$section_break")
	[Void]$ks_script_post.Add("# Specifies script to run after ESXi is installed and before reboot")
	[Void]$ks_script_post.Add("$section_break")
	[Void]$ks_script_post.Add("%post --interpreter=busybox --ignorefailure=true")
	[Void]$ks_script_post.Add("")

######## Set the 1st reboot
	[Void]$ks_script_1st_reboot.Add("$section_break")
	[Void]$ks_script_1st_reboot.Add("# Specifies script to run after ESXi installation and after first reboot")
	[Void]$ks_script_1st_reboot.Add("# Most of the shell command will enabled after the first reboot")
	[Void]$ks_script_1st_reboot.Add("$section_break")
	[Void]$ks_script_1st_reboot.Add("%firstboot --interpreter=busybox")
	[Void]$ks_script_1st_reboot.Add("")

######## Set Search domains vim-cmd?
	[Void]$ks_search_domains.Add("$section_break")
	[Void]$ks_search_domains.Add("## Set DNS Search Domains")
	[Void]$ks_search_domains.Add("$section_break")
	[Void]$ks_search_domains.Add("vim-cmd hostsvc/net/dns_set --searchdomain=$search_domains")
	[Void]$ks_search_domains.Add("")

### Research this section for better method, eliminate vim-cmd?
######### Set Remote SSH and Shell
	if ($dc_esxi_enable_ssh -eq "yes"){
		[Void]$ks_remote_SSH_Shell.Add("$section_break")
		[Void]$ks_remote_SSH_Shell.Add("# Enable Remote SSH")
		[Void]$ks_remote_SSH_Shell.Add("$section_break")
		[Void]$ks_remote_SSH_Shell.Add("vim-cmd hostsvc/enable_ssh")
		[Void]$ks_remote_SSH_Shell.Add("vim-cmd hostsvc/start_ssh")
		if ($dc_esxi_ssh_suppress_warn -eq "yes"){
		}
		[Void]$ks_remote_SSH_Shell.Add("")
	}

	if ($dc_esxi_enable_shell -eq "yes"){
		[Void]$ks_remote_SSH_Shell.Add("$section_break")
		[Void]$ks_remote_SSH_Shell.Add("# Enable Local Local Console")
		[Void]$ks_remote_SSH_Shell.Add("$section_break")
		[Void]$ks_remote_SSH_Shell.Add("vim-cmd hostsvc/enable_esx_shell")
		[Void]$ks_remote_SSH_Shell.Add("vim-cmd hostsvc/start_esx_shell")
		if ($dc_esxi_shell_suppress_warn -eq "yes"){
			[Void]$ks_remote_SSH_Shell.Add("# Suppress Shell Warning in Host")
			[Void]$ks_remote_SSH_Shell.Add("esxcli system settings advanced set -o /UserVars/SuppressShellWarning -i 1")
			[Void]$ks_remote_SSH_Shell.Add("esxcli system settings advanced set -o /UserVars/ESXiShellTimeOut -i 1")
		}
		[Void]$ks_remote_SSH_Shell.Add("")
	}

### General Advanced Settings Section
	[Void]$ks_advanced_settings.Add("$section_break")
	[Void]$ks_advanced_settings.Add("# Set NFS Advanced Settings")
	[Void]$ks_advanced_settings.Add("$section_break")
	[Void]$ks_advanced_settings.Add("esxcli system settings advanced set --option /Net/TcpipHeapSize --int-value $net_tcpipheapsize")
	[Void]$ks_advanced_settings.Add("esxcli system settings advanced set --option /Net/TcpipHeapMax --int-value $net_tcpipheapmax")
	[Void]$ks_advanced_settings.Add("esxcli system settings advanced set --option /NFS/HeartbeatMaxFailures --int-value $nfs_heartbeatmaxfailures")
	[Void]$ks_advanced_settings.Add("esxcli system settings advanced set --option /NFS/HeartbeatFrequency --int-value $nfs_heartbeatfrequency")
	[Void]$ks_advanced_settings.Add("esxcli system settings advanced set --option /NFS/HeartbeatTimeout --int-value $nfs_heartbeattimeout")
	[Void]$ks_advanced_settings.Add("esxcli system settings advanced set --option /NFS/MaxVolumes --int-value $nfs_maxvolumes")
	[Void]$ks_advanced_settings.Add("esxcli system settings advanced set --option /Net/BlockGuestBPDU --int-value 1")

######## Disable IPv6
	[Void]$ks_disable_ipv6.Add("$section_break")
	[Void]$ks_disable_ipv6.Add("# Disable ipv6")
	[Void]$ks_disable_ipv6.Add("$section_break")
	[Void]$ks_disable_ipv6.Add("esxcli system module parameters set -m tcpip4 -p ipv6=0")
	[Void]$ks_disable_ipv6.Add("")

######## Set Mgmt back on vmk0
	[Void]$ks_add_mgmt_vmk0.Add("$section_break")
	## Add Management to VMK0 (find replacement for better method)
	[Void]$ks_add_mgmt_vmk0.Add("$section_break")
	[Void]$ks_add_mgmt_vmk0.Add("/etc/init.d/hostd stop")
	[Void]$ks_add_mgmt_vmk0.Add("sleep 5")
	[Void]$ks_add_mgmt_vmk0.Add("cat > /etc/vmware/hostd/hostsvc.xml << __HOSTSVC_XML__")
	[Void]$ks_add_mgmt_vmk0.Add("<ConfigRoot>")
	[Void]$ks_add_mgmt_vmk0.Add("  <mangementVnics>")
	[Void]$ks_add_mgmt_vmk0.Add('    <nic id="0000">vmk0</nic>')
	[Void]$ks_add_mgmt_vmk0.Add("  </mangementVnics>")
	[Void]$ks_add_mgmt_vmk0.Add("  <mode>normal</mode>")
	[Void]$ks_add_mgmt_vmk0.Add("  <service>")
	[Void]$ks_add_mgmt_vmk0.Add("    <TSM>on</TSM>")
	[Void]$ks_add_mgmt_vmk0.Add("    <TSM-SSH>on</TSM-SSH>")
	[Void]$ks_add_mgmt_vmk0.Add("    <ntpd>on</ntpd>")
	[Void]$ks_add_mgmt_vmk0.Add("  </service>")
	[Void]$ks_add_mgmt_vmk0.Add("</ConfigRoot>")
	[Void]$ks_add_mgmt_vmk0.Add("__HOSTSVC_XML__")
	[Void]$ks_add_mgmt_vmk0.Add("")
	[Void]$ks_add_mgmt_vmk0.Add("/etc/init.d/hostd start")
	[Void]$ks_add_mgmt_vmk0.Add("")
	
	
######## Set NTP 
######## This section likely needs to be updated
	[Void]$ks_ntp.Add("$section_break")
	[Void]$ks_ntp.Add("##  NTP Configuration")
	[Void]$ks_ntp.Add("$section_break")
	[Void]$ks_ntp.Add("cp /etc/ntp.conf /etc/ntp.conf.orig")
	[Void]$ks_ntp.Add("cat << EOF  > /etc/ntp.conf")
	[Void]$ks_ntp.Add("restrict default kod nomodify notrap nopeer")
	[Void]$ks_ntp.Add("restrict 127.0.0.1")
		Foreach($ntp_server_name in $ntp_server_names)
		{
			[Void]$ks_ntp.Add("server $ntp_server_name")
		}
	[Void]$ks_ntp.Add("driftfile /etc/ntp.drift")
	[Void]$ks_ntp.Add("EOF")
	[Void]$ks_ntp.Add("")
	[Void]$ks_ntp.Add("# Set NTP service settings")
	[Void]$ks_ntp.Add("/sbin/chkconfig --level 345 ntpd on")
	[Void]$ks_ntp.Add("")

######## Set Firewall Services
	$esxi_firewall_services_enable = "syslog","sshClient","ntpClient","updateManager","httpClient","netdump"
	[Void]$ks_firewall.Add("$section_break")
	[Void]$ks_firewall.Add("##  Enable Firewall")
	[Void]$ks_firewall.Add("$section_break")
	[Void]$ks_firewall.Add("esxcli network firewall set --default-action false --enabled yes")
	foreach ($firewall_service in $esxi_firewall_services_enable) {
		[Void]$ks_firewall.Add("esxcli network firewall ruleset set --ruleset-id $firewall_service --enabled yes")
	}
	[Void]$ks_firewall.Add("")

######## Set Wrap Up stuff
######## This Section needs to be updated for the new way we do things here!
	[Void]$ks_wrap_up.Add("$section_break")
	[Void]$ks_wrap_up.Add("##  Put to Maintenance Mode, backup logs and Reboot")
	[Void]$ks_wrap_up.Add("$section_break")
	[Void]$ks_wrap_up.Add("# backup ESXi configuration to persist changes")
	[Void]$ks_wrap_up.Add("/sbin/auto-backup.sh")
	[Void]$ks_wrap_up.Add("# enter maintenance mode")
	[Void]$ks_wrap_up.Add("# vim-cmd hostsvc/maintenance_mode_enter")
	[Void]$ks_wrap_up.Add("# copy %first boot script logs to persisted datastore")
	[Void]$ks_wrap_up.Add('cp /var/log/hostd.log "/vmfs/volumes/local_$(hostname -s)/firstboot-hostd.log"')
	[Void]$ks_wrap_up.Add('cp /var/log/esxi_install.log "/vmfs/volumes/local_$(hostname -s)/firstboot-esxi_install.log"')
	[Void]$ks_wrap_up.Add("reboot")
	[Void]$ks_wrap_up.Add("$section_break")
	[Void]$ks_wrap_up.Add("##  End of kickstart Script")
	[Void]$ks_wrap_up.Add("$section_break")


##### iso menu  variables
	[Void]$iso_menu.Add("DEFAULT menu.c32")
	[Void]$iso_menu.Add("MENU TITLE $customer_name Boot Menu")
	[Void]$iso_menu.Add("NOHALT 1")
	[Void]$iso_menu.Add("PROMPT 0")
	[Void]$iso_menu.Add("TIMEOUT 0")
	[Void]$iso_menu.Add("LABEL install")
	[Void]$iso_menu.Add("  KERNEL mboot.c32")
	[Void]$iso_menu.Add("  APPEND -c boot.cfg")
	[Void]$iso_menu.Add("  MENU LABEL OpenOffice ^Installer")
	[Void]$iso_menu.Add("LABEL hddboot")
	[Void]$iso_menu.Add("  LOCALBOOT 0x80")
	[Void]$iso_menu.Add("  MENU LABEL ^Boot from local disk")
	[Void]$iso_menu.Add("")
	[Void]$iso_menu.Add("LABEL -")
	[Void]$iso_menu.Add("        menu label ^Automation Written by Datalink")
	[Void]$iso_menu.Add("        menu disable")
	[Void]$iso_menu.Add("")
	[Void]$iso_menu.Add("LABEL - vSphere ClusterName")
	[Void]$iso_menu.Add("        menu label ^$global_esxi_cluster_name Hosts:")
	[Void]$iso_menu.Add("        menu disable")
	[Void]$iso_menu.Add("")


############################ Each host Section ############################
Foreach($esxi_host in $esxi_hosts)
{	
	# Clear objects for host loop
	$ks_title.Clear()
	$ks_vswitch_Config.Clear()
	$ks_portgroup_Config.Clear()
	$ks_rename_ds.Clear()
	$ks_default_mgmt_int_data.Clear()
	# Added by RD 2-13-2015
	$ks_vsan_config.Clear()
	#
	# Set variable values per host
	# set the host specific values 
	### Might have to rewrite this section to remove roles from vmks
	$esxi_hostname_fqdn = $esxi_host.esxi_hostname_fqdn
	$esxi_hostname_short = $esxi_hostname_fqdn.Split(".")[0]
	$esxi_enable_vsan = $esxi_host.esxi_enable_vsan
	$esxi_vmk0_mgmt_ip	= $esxi_host.esxi_vmk0_ip
	$esxi_vmk0_mgmt_nm	= $esxi_host.esxi_vmk0_nm
	$esxi_vmk0_mgmt_gw	= $esxi_host.esxi_vmk0_gw
	
	[Void]$ks_title.Add("$section_break")
	[Void]$ks_title.Add("### Kickstart Created by Datalink Automation Engineering")
	[Void]$ks_title.Add("### Hostname: $esxi_hostname_fqdn")
	[Void]$ks_title.Add("$section_break")
	
	######## Rename the local datastore
	[Void]$ks_rename_ds.Add("$section_break")
	[Void]$ks_rename_ds.Add("# Rename datastore1 to the prefix loaded from the system")
	[Void]$ks_rename_ds.Add("$section_break")
	[Void]$ks_rename_ds.Add("vim-cmd hostsvc/datastore/rename datastore1 " + $dc_local_vmfs_prefix + $esxi_hostname_short)
	[Void]$ks_rename_ds.Add("")
	
	# Perform variable validation or calculation for advanced options for HOST LOOP
	#### need to modify to remove hard code vmnic1 for vSwitch0 configuration.
	#### Thi entire section may be rewritten pending on how things go with new modular automation requests.
	#### Don't tell me how to do it...yet
	Foreach($vswitch in $vswitch_data)
	{
		$vswitch_name = $vswitch.esxi_vswitch_name
		$vswitch_ports = $vswitch.esxi_vswitch_num_ports
		$vswitch_mtu = $vswitch.esxi_vswitch_mtu
		$vswitch_act_uplinks = $vswitch.esxi_vswitch_active_uplinks
		$vswitch_act_uplinks_list = $vswitch_act_uplinks.Split(",")
		$vswitch_cdp_status = $vswitch.cdp_status
		[Void]$ks_vswitch_config.Add("$section_break")
		[Void]$ks_vswitch_config.Add("#### Start $vswitch_name section")
		[Void]$ks_vswitch_config.Add("$section_break")
		# This should change the logic on how it grabs the vSwitch for Management interface.
		# rewrite that and the vmnic1 hard code.
		
		if ($vswitch_name -eq "vSwitch0")
			{
				# vSwitch0 get's special treatment - as the installation nic is already added (vmnic0 typically)
				Foreach($vswitch_uplink in $vswitch_act_uplinks_list) {
					if ($vswitch_uplink -ne "vmnic0") {
						[Void]$ks_vswitch_config.Add("esxcli network vswitch standard uplink add --uplink-name $vswitch_uplink --vswitch-name $vswitch_name")
					}
				}
				#[Void]$ks_vswitch_config.Add("esxcli network vswitch standard set --ports $vswitch_ports --vswitch-name $vswitch_name")
				[Void]$ks_vswitch_config.Add("esxcli network vswitch standard policy failover set --active-uplinks $vswitch_act_uplinks --vswitch-name $vswitch_name")
				[Void]$ks_vswitch_config.Add("esxcli network vswitch standard policy failover set --failure-detection link --failback yes --notify-switches yes --vswitch-name $vswitch_name")
			}
		Else 
			{
				[Void]$ks_vswitch_config.Add("esxcli network vswitch standard add --ports $vswitch_ports --vswitch-name $vswitch_name")
				Foreach($vswitch_uplink in $vswitch_act_uplinks_list)
					{
						[Void]$ks_vswitch_config.Add("esxcli network vswitch standard uplink add --uplink-name $vswitch_uplink --vswitch-name $vswitch_name")
					}
				[Void]$ks_vswitch_config.Add("esxcli network vswitch standard policy failover set --active-uplinks $vswitch_act_uplinks --vswitch-name $vswitch_name")
				[Void]$ks_vswitch_config.Add("esxcli network vswitch standard policy failover set --failure-detection link --failback yes --notify-switches yes --vswitch-name $vswitch_name")
				[Void]$ks_vswitch_config.Add("# esxcli network vswitch standard policy security set --allow-forged-transmits yes --allow-mac-change yes --allow-promiscuous no --vswitch-name $vswitch_name")

			
			}
		[Void]$ks_vswitch_config.Add("esxcli network vswitch standard set --mtu $vswitch_mtu --cdp-status $vswitch_cdp_status --vswitch-name $vswitch_name")
		[Void]$ks_vswitch_config.Add("$section_break")
		[Void]$ks_vswitch_config.Add("#### End $vswitch_name section")
		[Void]$ks_vswitch_config.Add("$section_break")
		[Void]$ks_vswitch_config.Add("")
	}
		
	####### VMK Portgroup Section
	# Set Default Management VLAN and add the second vmnic to the management port as active / active.
	## Rewrite VLAN for management portion
	######### Lots of Rewrite options here
	[Void]$ks_portgroup_config.Add("$section_break")
	[Void]$ks_portgroup_config.Add("# Configure Management Network failover policy since adding vmnic to vSwitch0")
	[Void]$ks_portgroup_config.Add("# Set policy to active / active")
	[Void]$ks_portgroup_config.Add("# REM this line out if you want vmk0 ACTIVE / STANDBY with vmnic0 ACTIVE")
	[Void]$ks_portgroup_config.Add("$section_break")

	# If multi-nic vmotion is used, then we loop and pin the vmk's on the vmotion switch
	Foreach($vmk_port in $vmk_port_data)
		{
		$vmk_name = $vmk_port.vmk_num
		$vmk_ip = ($esxi_host | select ("esxi_"+"$vmk_name"+"_ip")).("esxi_"+"$vmk_name"+"_ip")
		$vmk_nm = ($esxi_host | select ("esxi_"+"$vmk_name"+"_nm")).("esxi_"+"$vmk_name"+"_nm")
		$vmk_pg_name = $vmk_port.vmk_pg_name
		$vmk_mtu = $vmk_port.vmk_mtu
		$vmk_vlan = $vmk_port.vmk_vlan
		$vmk_role = $vmk_port.vmk_role
		$vmk_vswitch = $vmk_port.vswitch
		$vmk_act_uplinks = $vmk_port.vmk_act_uplinks
		$vmk_stby_uplinks = $vmk_port.vmk_stby_uplinks
		$vmkFailoverOrder=""

		if ($vmk_act_uplinks) {$vmkFailoverOrder+=" -a $vmk_act_uplinks"}
		if ($vmk_stby_uplinks) {$vmkFailoverOrder+=" -s $vmk_stby_uplinks"}
		
			if($vmk_port.vmk_num -eq "vmk0")
			{
				#[Void]$ks_default_mgmt_int.Add("")
				[Void]$ks_portgroup_config.Add('esxcli network vswitch standard portgroup policy failover set -p "Management Network" $vmkFailoverOrder')
								# Set command strings into sections with resultant values
				$vmk_vlan = $vmk_port.vmk_vlan
				if($vmk_vlan -eq "")
					{
						[Void]$ks_default_mgmt_int_data.Add("network --bootproto=$ks_boot_proto --device=$esxi_mgmt_build_vmnic --ip=$esxi_vmk0_mgmt_ip --netmask=$esxi_vmk0_mgmt_nm --gateway=$esxi_vmk0_mgmt_gw --hostname=$esxi_hostname_fqdn --nameserver=$dns_server_ips_str --addvmportgroup=0")
						[Void]$ks_default_mgmt_int_data.Add("")
						[Void]$ks_portgroup_config.Add('# needs update for DAF # esxcli network vswitch standard portgroup set -p "Management Network"')
					}
				elseif($vmk_vlan -ne "")
					{
						[Void]$ks_default_mgmt_int_data.Add("network --bootproto=$ks_boot_proto --device=$esxi_mgmt_build_vmnic --ip=$esxi_vmk0_mgmt_ip --netmask=$esxi_vmk0_mgmt_nm --gateway=$esxi_vmk0_mgmt_gw --vlanid=$vmk_vlan --hostname=$esxi_hostname_fqdn --nameserver=$dns_server_ips_str --addvmportgroup=0")
						[Void]$ks_portgroup_config.Add('esxcli network vswitch standard portgroup set -p "Management Network"' + " --vlan-id=$vmk_vlan")
					}
				[Void]$ks_portgroup_config.Add("")
			}
			Else
			{
				[Void]$ks_portgroup_config.Add("$section_break")
				[Void]$ks_portgroup_config.Add("# $vmk_role vmkernel $vmk_name")
				[Void]$ks_portgroup_config.Add("$section_break")
				[Void]$ks_portgroup_config.Add("esxcli network vswitch standard portgroup add --portgroup-name $vmk_pg_name --vswitch-name $vmk_vswitch")
				[Void]$ks_portgroup_config.Add('esxcli network vswitch standard portgroup policy failover set -p $vmk_pg_name $vmkFailoverOrder')
				[Void]$ks_portgroup_config.Add("esxcli network ip interface add --interface-name $vmk_name --mtu $vmk_mtu --portgroup-name $vmk_pg_name")
				if($vmk_vlan -ne $null){[Void]$ks_portgroup_config.Add("esxcli network vswitch standard portgroup set --portgroup-name=$vmk_pg_name --vlan-id=$vmk_vlan")}
				Elseif($vmk_vlan -eq $null){[Void]$ks_portgroup_config.Add("esxcli network vswitch standard portgroup set --portgroup-name=$vmk_pg_name")}
				[Void]$ks_portgroup_config.Add("esxcli network ip interface ipv4 set --interface-name $vmk_name --ipv4 $vmk_ip --netmask $vmk_nm --type static")
				switch ($vmk_role) {
					"management" {[Void]$ks_portgroup_config.Add("esxcli network ip interface tag add -i $vmk_name -t Management")}
					"vmotion" {[Void]$ks_portgroup_config.Add("esxcli network ip interface tag add -i $vmk_name -t VMotion")}
					"ft" {[Void]$ks_portgroup_config.Add("esxcli network ip interface tag add -i $vmk_name -t faultToleranceLogging")}
					"vsan" {[Void]$ks_portgroup_config.Add("esxcli vsan network ipv4 add -i $vmk_name")}
				}
				
				$vswitch_failover_cmd = vSwitch-FailoverCmd $vmk_port
				[Void]$ks_portgroup_config.Add("$vswitch_failover_cmd")
			}
		}

	# VM PortGroups
	if ($vm_portgroup_data) {
		[Void]$ks_portgroup_config.Add("$section_break")
		[Void]$ks_portgroup_config.Add("## VM Portgroups ##")
		[Void]$ks_portgroup_config.Add("$section_break")
		Foreach($vm_portgroup in $vm_portgroup_data)
			{
				$esxi_pg_name = $vm_portgroup.esxi_pg_name	
				$esxi_pg_vlan = $vm_portgroup.esxi_pg_vlan
				$esxi_pg_vswitch = $vm_portgroup.esxi_pg_vswitch
				[Void]$ks_portgroup_config.Add("esxcli network vswitch standard portgroup add --portgroup-name=$esxi_pg_name --vswitch-name=$esxi_pg_vswitch")
				[Void]$ks_portgroup_config.Add("esxcli network vswitch standard portgroup set --portgroup-name=$esxi_pg_name --vlan-id=$esxi_pg_vlan")
			}
		[Void]$ks_portgroup_config.Add("$section_break")
		[Void]$ks_portgroup_config.Add("")
	}
	
	# VM VSAN Config
	if ($esxi_enable_vsan -eq "yes") {
		[Void]$ks_vsan_config.Add("$section_break")
		[Void]$ks_vsan_config.Add("# Configure VSAN")
		[Void]$ks_vsan_config.Add("$section_break")
		[Void]$ks_vsan_config.Add("# Set default VSAN policy")
		[Void]$ks_vsan_config.Add("esxcli vsan policy setdefault -c vdisk -p ""((\""hostFailuresToTolerate\"" i1) (\""forceProvisioning\"" i1))"" ")
		[Void]$ks_vsan_config.Add("esxcli vsan policy setdefault -c vmnamespace -p ""((\""hostFailuresToTolerate\"" i1) (\""forceProvisioning\"" i1))"" ")
		[Void]$ks_vsan_config.Add("")
		[Void]$ks_vsan_config.Add("def createVsanDiskGroup():")
		[Void]$ks_vsan_config.Add(" vdqoutput = eval(commands.getoutput(""/sbin/vdq -q""))")
		[Void]$ks_vsan_config.Add(" md = []")
		[Void]$ks_vsan_config.Add(" ssd = ''")
		[Void]$ks_vsan_config.Add(" for i in vdqoutput:")
		[Void]$ks_vsan_config.Add(" if i['State'] == 'Eligible for use by VSAN':")
		[Void]$ks_vsan_config.Add(" if i['Reason'] == 'Non-local disk':")
		[Void]$ks_vsan_config.Add(" if debug == False:")
		[Void]$ks_vsan_config.Add(" os.system(""esxcli storage nmp satp rule add -s VMW_SATP_LOCAL -o enable_local -d "" + i['Name'])")
		[Void]$ks_vsan_config.Add(" os.system(""esxcli storage core claiming reclaim -d "" + i['Name'])")
		[Void]$ks_vsan_config.Add(" if i['IsSSD'] == '1':")
		[Void]$ks_vsan_config.Add(" ssd = i['Name']")
		[Void]$ks_vsan_config.Add(" else:")
		[Void]$ks_vsan_config.Add(" md.append(i['Name'])")
		[Void]$ks_vsan_config.Add(" diskgroup_cmd = 'esxcli vsan storage add -s ' + ssd + ' -d ' + ' -d '.join(md)")
		[Void]$ks_vsan_config.Add(" if debug == False:")
		[Void]$ks_vsan_config.Add(" os.system(diskgroup_cmd)"" ")
		[Void]$ks_vsan_config.Add("")
		[Void]$ks_vsan_config.Add("# Create VSAN Cluster")		
		[Void]$ks_vsan_config.Add("def createVsanCluster():")
		[Void]$ks_vsan_config.Add(" # generate UUID for VSAN Cluster")
		[Void]$ks_vsan_config.Add(" vsan_uuid = str(uuid.uuid4())")
		[Void]$ks_vsan_config.Add(" if debug == False:")
		[Void]$ks_vsan_config.Add(" os.system(""esxcli vsan cluster join -u "" + vsan_uuid)")
		[Void]$ks_vsan_config.Add("")
		[Void]$ks_vsan_config.Add("createVsanDiskGroup()")
		[Void]$ks_vsan_config.Add("createVsanCluster()")
		[Void]$ks_vsan_config.Add("vsan storage automode set --enabled")
	}
	
	# Combine the sections to form the output
	$ks_all_cmds = $ks_title + 
	$ks_header + 
	$ks_disk_partitioning + $ks_install_media +
	$ks_root_password +
	$ks_default_mgmt_int +
	$ks_default_mgmt_int_data +
	$ks_reboot_after_install_flag +
	$ks_script_pre +
	$ks_script_post +
	$ks_script_1st_reboot +
	$ks_vswitch_config +
	$ks_portgroup_config +
	$ks_rename_ds +
	$ks_vsan_config +
	$ks_remote_ssh_shell +
	#$ks_add_mgmt_vmk0 +
	$ks_disable_ipv6 +
	$ks_advanced_settings +
	$ks_ntp +
	$ks_firewall +
	$ks_search_domains +
	$ks_wrap_up 
	 

# output the file
	$iso_ks_name = $esxi_hostname_short.ToUpper()
	$ks_all_cmds | Out-File "$esxi_cmds_path\$iso_ks_name.CFG" -Encoding ASCII

# Clear up host specific stuff
	[Void]$ks_default_mgmt_int.Remove("network --bootproto=$ks_boot_proto --device=$esxi_mgmt_build_vmnic --ip=$esxi_vmk0_mgmt_ip --netmask=$esxi_vmk0_mgmt_nm --gateway=$esxi_vmk0_mgmt_gw --hostname=$esxi_hostname_fqdn --nameserver=$dns1,$dns2 --addvmportgroup=0")
	[Void]$ks_default_mgmt_int.Remove("")
	
# Create the ISO menu for the host
	[Void]$iso_menu.Add("LABEL $iso_ks_name") 
	[Void]$iso_menu.Add("	menu label `"$iso_ks_name`"")
	[Void]$iso_menu.Add("	menu indent 2") 
	[Void]$iso_menu.Add("	kernel mboot.c32")
	[Void]$iso_menu.Add("	append vmkboot.gz ks=cdrom:/KS/$iso_ks_name.CFG --- vmkernel.gz --- sys.vgz --- cim.vgz --- ienviron.vgz --- install.vgz")
	[Void]$iso_menu.Add("")

}

$iso_menu | Out-File "$esxi_cmds_path\$iso_menu_filename" -Encoding ASCII


If($global_esxi_src_iso -ne "")
	{
		echo "Building is based on $global_esxi_src_iso"
		Create-ESXiISO -customer $customer_name -build_id $build_id -iso_src_dir_name $global_esxi_src_iso
	}