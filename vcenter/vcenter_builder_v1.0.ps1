Param(
  [Parameter(Mandatory=$True,Position=1)]
  [string]$datasource_name,
  [Parameter(Mandatory=$True,Position=2)]
  [string]$build_cmds,
  [Parameter(Mandatory=$True,Position=3)]
  [string]$build_docs,
  [Parameter(Mandatory=$True,Position=4)]
  [string]$build_id
  
)

# Create the Name of rhis script
$this_script = ($MyInvocation.MyCommand).Name

# Define Global Objects for As Built
$as_built_build_id						= New-Object System.Collections.ArrayList
$as_built_customer						= New-Object System.Collections.ArrayList
$as_built_networks						= New-Object System.Collections.ArrayList

# Define Global Data Values from the Globals Spreadsheet
#region Get global data
$global_customer | %{			
	$customer_name			= $_.customer_name
	$contact_name			= $_.contact_name
	$contact_email			= $_.contact_email
	$contact_phone			= $_.contact_phone
	}
$global_location | %{
	$customer_address		= $_.customer_address
	$system_install_address	= $_.system_install_address
	$rma_address			= $_.rma_address
	$rma_attn_to_name		= $_.rma_attn_to_name
	}
$global_datacenter | %{
	$datacenter_name			= $_.datacenter_name
	$domain_name				= $_.domain_name
	$dns_server_ips_str			= $_.dns_server_ips
	$ntp_server_names_str		= $_.ntp_server_names
	$smtp_server_name			= $_.smtp_server_name
	$snmp_server_name			= $_.snmp_server_name
	$snmp_community_string		= $_.snmp_community_string
	$syslog_server_names_str	= $_.syslog_server_name #Added RD 2/20/2015
	$timezone					= $_.timezone
	$default_admin_user			= $_.default_admin_user
	$default_admin_pass			= $_.default_admin_pass
	$domain_join_user			= $_.domain_join_user
	$domain_join_pass			= $_.domain_join_pass

	if ($dns_server_ips_str -match ",")
		{
		$dns_server_ips		= $dns_server_ips_str.Split(",")
		}
	else
		{
		$dns_server_ips		= @($dns_server_ips_str)
		}
	
	if ($ntp_server_names_str -match ",")
		{
		$ntp_server_names	= $ntp_server_names_str.Split(",")
		}
	else
		{
		$ntp_server_names	= @($ntp_server_names_str)
		}
	if ($syslog_server_names_str -match ",") #Added RD 2/20/2015
		{
		$syslog_server_names	= $syslog_server_names_str.Split(",")
		}
	else
		{
		$syslog_server_names	= @($syslog_server_names_str)
		}
	}
	
	
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Contact Name"
$as_built_customer_obj.customer_value	= $contact_name
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Contact Email"
$as_built_customer_obj.customer_value	= $contact_email
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Contact Phone"
$as_built_customer_obj.customer_value	= $contact_phone
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Customer Address"
$as_built_customer_obj.customer_value	= $customer_address
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "System Install Address"
$as_built_customer_obj.customer_value	= $system_install_address	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Rma Address"
$as_built_customer_obj.customer_value	= $rma_address	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Rma Attn To Name"
$as_built_customer_obj.customer_value	= $rma_attn_to_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Domain Name"
$as_built_customer_obj.customer_value	= $domain_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Dns Server Ips"
$as_built_customer_obj.customer_value	= $dns_server_ips
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Ntp Server Names"
$as_built_customer_obj.customer_value	= $ntp_server_names	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Smtp Server Name"
$as_built_customer_obj.customer_value	= $smtp_server_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Snmp Server Name"
$as_built_customer_obj.customer_value	= $snmp_server_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Snmp Community String"
$as_built_customer_obj.customer_value	= $snmp_community_string
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Timezone"
$as_built_customer_obj.customer_value	= $timezone	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Default Admin User"
$as_built_customer_obj.customer_value	= $default_admin_user	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Default Admin Pass"
$as_built_customer_obj.customer_value	= $default_admin_pass	
[Void]$as_built_customer.Add($as_built_customer_obj)

#endregion

###### Set Build_ID Data
#Define vCenter Constants variables
$psLoadModules = @("VMware.VimAutomation.Core")
$vcenterTaskRetention=180
$vcenterTaskRetentionEnabled=$false
$vcenterEventRetention=180
$vcenterEventRetentionEnabled=$false
$esxiHostDefaultShellTimeout=900
$esxiHostDefaultSshTimeout=900
$esxiHostDefaultSshTimeout=900	
$secureStringKey = (2,3,56,34,254,222,1,1,2,23,42,54,33,233,1,34,2,7,6,5,35,43,6,6,6,6,6,6,31,33,60,23)

###### Set Build_ID Data
$vcenter 			= $vcenter_data.get_item("vcenter_vcsa")
$cluster 			= $vcenter_data.get_item("vcenter_cluster")
$clusterHosts 		= $vcenter_data.get_item("vcenter_cluster_hosts")
$dvSwitches 		= $vcenter_data.get_item("vcenter_dvswitch")
$dvSwitchPortGroups = $vcenter_data.get_item("vcenter_dvswitch_portgroups")
$folders 			= $vcenter_data.get_item("vcenter_folders")
$alarms 			= $vcenter_data.get_item("vcenter_alarms")
$virtualAppliances	= $vcenter_data.get_item("vcenter_vapps")

###### Set vCenter Global Data
$vcenter_fqdn = $vcenter.vcenter_fqdn
$vcenter_admin_password = $vcenter.vcenter_admin_password
$vcenter_version = $vcenter.vcenter_version
$vcenter_appliance_size = $vcenter.vcenter_appliance_size
$vcenter_esxi_host_fqdn = $vcenter.vcenter_esxi_host_fqdn
$vcenter_esxi_host_password = $vcenter.vcenter_esxi_host_password
$vcenter_ip_address = $vcenter.vcenter_ip_address
$vcenter_subnet_mask = $vcenter.vcenter_subnet_mask
$vcenter_gateway = $vcenter.vcenter_gateway
$vcenter_dns_search_domains = $vcenter.vcenter_dns_search_domains
$vcenter_port_group = $vcenter.vcenter_port_group
$vcenter_datastore = $vcenter.vcenter_datastore
$vcenter_db_type = $vcenter.vcenter_db_type
$vcenter_sso_type = $vcenter.vcenter_sso_type
$vcenter_sso_password = $vcenter.vcenter_sso_password
$vcenter_ad_join = $vcenter.vcenter_ad_join
$vcenter_smtp_from_addr = $vcenter.vcenter_smtp_from_addr
$vcenter_licence_keys = $vcenter.vcenter_licence_keys

if ($vcenter_version -ge 5.5) {
	$vcsaMinDiskSpace = 70
} else {
	$vcsaMinDiskSpace = 7
}

switch ($vcenter_appliance_size) {
	"large" {
		$vcenterApplianceCpu 			= 16
		$vcenterApplianceRamGb 			= 32
		$vcenterApplianceTcserverMb 	= 1024
		$vcenterApplianceInvSvcGb		= 12
		$vcenterAppliancePDSorageGb		= 4
		$vcenterMonLevel1				= 1
		$vcenterMonLevel2				= 1
		$vcenterMonLevel3				= 1
		$vcenterMonLevel4				= 1

		break;
	}
	"medium" {
		$vcenterApplianceCpu 			= 8
		$vcenterApplianceRamGb 			= 24
		$vcenterApplianceTcserverMb 	= 512
		$vcenterApplianceInvSvcGb		= 6
		$vcenterAppliancePDSorageGb		= 1
		$vcenterMonLevel1				= 2
		$vcenterMonLevel2				= 2
		$vcenterMonLevel3				= 2
		$vcenterMonLevel4				= 2
		break;
	}
	default {
		$vcenterApplianceCpu 			= 4
		$vcenterApplianceRamGb 			= 8
		$vcenterApplianceTcserverMb 	= 512
		$vcenterApplianceInvSvcGb		= 3
		$vcenterAppliancePDSorageGb		= 2
		$vcenterMonLevel1				= 2
		$vcenterMonLevel2				= 2
		$vcenterMonLevel3				= 2
		$vcenterMonLevel4				= 2
		break;
	}
}



######### Define Global Sections
$scriptOptions									= New-Object System.Collections.ArrayList
$scriptInputs									= New-Object System.Collections.ArrayList
$vcenterVappDeployment							= New-Object System.Collections.ArrayList
$vcenterVappConfig								= New-Object System.Collections.ArrayList
$vcenterBaseConfig								= New-Object System.Collections.ArrayList
$vcenterCluster									= New-Object System.Collections.ArrayList
$vcenterNetworking								= New-Object System.Collections.ArrayList
$vcenterPermissions								= New-Object System.Collections.ArrayList
$vcenterFolders									= New-Object System.Collections.ArrayList
$vcenterAlarms									= New-Object System.Collections.ArrayList
$vcenterHardening								= New-Object System.Collections.ArrayList
$vcenterDisconnect								= New-Object System.Collections.ArrayList
$outputScript									= @()

############### Set Global PowerCLI Options
[Void]$scriptOptions.Add($section_break)
[Void]$scriptOptions.Add("# Set Global Powershell Options")
[Void]$scriptOptions.Add($section_break)
[Void]$scriptOptions.Add("Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Scope Session -Confirm:`$false | Out-Null")
[Void]$scriptOptions.Add("`$ErrorActionPreference = ""Stop""")
[Void]$scriptOptions.Add("`$ovftoolpaths = ""C:\Program Files (x86)\VMware\VMware OVF Tool\ovftool.exe"",""C:\Program Files\VMware\VMware OVF Tool\ovftool.exe""")
[Void]$scriptOptions.Add("`$secureStringKey = (2,3,56,34,254,222,1,1,2,23,42,54,33,233,1,34,2,7,6,5,35,43,6,6,6,6,6,6,31,33,60,23)")
foreach ($psLoadModule in $psLoadModules) {
	[Void]$scriptOptions.Add("if ( (Get-PSSnapin -Name $psLoadModule -ErrorAction SilentlyContinue) -eq `$null ) {")
	[Void]$scriptOptions.Add("	Add-PSSnapin $psLoadModule")
	[Void]$scriptOptions.Add("}")	
}

if (($global_deploy_vcenter -eq "yes") -Or ($global_configure_cluster -eq "yes")) {
	############### Gather Credentials
	[Void]$scriptInputs.Add($section_break)
	[Void]$scriptInputs.Add("# Gather Credentials")
	[Void]$scriptInputs.Add($section_break)
	# Collect passwords needed during script runtime
	[Void]$scriptInputs.Add($carriage_return)
	[Void]$scriptInputs.Add("Write-Host ""`r""")
	[Void]$scriptInputs.Add("Write-Host -ForegroundColor Yellow ""Please enter the following information;"" ")
	[Void]$scriptInputs.Add($carriage_return)

	#If password is defined in xls file it is encrypted and written out.
	#if not, a prompt is added which displayed during runtime for root password 
	#for ESXi server that VCSA will be initially deployed into
	[Void]$scriptInputs.Add($carriage_return)
	if ($default_admin_pass) {
		$secureEsxiRootPassword = $default_admin_pass | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString -Key $secureStringKey 
		[Void]$scriptInputs.Add("`$secureEsxiRootPassword = """ + $secureEsxiRootPassword + """ | ConvertTo-SecureString -Key `$secureStringKey")
		[Void]$scriptInputs.Add("`$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$secureEsxiRootPassword)")
		[Void]$scriptInputs.Add("`$plainEsxiRootPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto(`$BSTR)")
		[Void]$scriptInputs.Add("`$esxiUsername = ""root""")
		[Void]$scriptInputs.Add("[System.Reflection.Assembly]::LoadWithPartialName(""System.Web"") | out-null")
		[Void]$scriptInputs.Add("`$httpEsxiRootPassword = [System.Web.HttpUtility]::UrlEncode(`$plainEsxiRootPassword)")
		[Void]$scriptInputs.Add("`$esxiCredentials = New-Object System.Management.Automation.PSCredential -ArgumentList `$esxiUsername, `$secureEsxiRootPassword")
	} else {
		$passwordPrompt="ESXi root password"
		[Void]$scriptInputs.Add("do {
	`$passwordCorrect=`$false
	`$esxiRootPassword1 = Read-Host -Prompt ""Enter $passwordPrompt."" -AsSecureString
	`$esxiRootPassword2 = Read-Host -Prompt ""Re-enter $passwordPrompt."" -AsSecureString
	if ([Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$esxiRootPassword1)) -eq [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$esxiRootPassword2))) {
		`$secureEsxiRootPassword = `$esxiRootPassword1
		`$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$secureEsxiRootPassword)
		`$plainEsxiRootPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto(`$BSTR)
		[System.Reflection.Assembly]::LoadWithPartialName(""System.Web"") | out-null
		`$httpEsxiRootPassword = [System.Web.HttpUtility]::UrlEncode(`$plainEsxiRootPassword)
		`$passwordCorrect=`$true
		`$esxiUsername = ""root""
		`$esxiCredentials = New-Object System.Management.Automation.PSCredential -ArgumentList `$esxiUsername, `$secureEsxiRootPassword
	} else {
		Write-Host -ForegroundColor Red ""$passwordPrompt  did not match. Please re-enter.""
	}
} while (`$passwordCorrect -ne `$true)")
	}

	#Prompts during runtime for administrator@vsphere.local password for VCSA configuration
	[Void]$scriptInputs.Add($carriage_return)
	if ($vcenter_sso_password) {
		$secureVcenterAdminPassword = $vcenter_sso_password | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString -Key $secureStringKey 
		[Void]$scriptInputs.Add("`$secureVcenterAdminPassword = """ + $secureVcenterAdminPassword + """ | ConvertTo-SecureString -Key `$secureStringKey")
		[Void]$scriptInputs.Add("`$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$secureVcenterAdminPassword)")
		[Void]$scriptInputs.Add("`$plainVcenterAdminPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto(`$BSTR)")
		[Void]$scriptInputs.Add("`$vcenterUsername = ""administrator@vsphere.local""")
		[Void]$scriptInputs.Add("`$vcenterCredentials = New-Object System.Management.Automation.PSCredential -ArgumentList `$vcenterUsername, `$secureVcenterAdminPassword")
	} else {
	$passwordPrompt="administrator@vsphere.local password"
	[Void]$scriptInputs.Add("do {
	`$passwordCorrect=`$false
	`$vcenterAdminPassword1 = Read-Host -Prompt ""Enter $passwordPrompt."" -AsSecureString
	`$vcenterAdminPassword2 = Read-Host -Prompt ""Re-enter $passwordPrompt."" -AsSecureString
	if ([Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$vcenterAdminPassword1)) -eq [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$vcenterAdminPassword2))) {
		`$secureVcenterAdminPassword = `$vcenterAdminPassword1
		`$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$secureVcenterAdminPassword)
		`$plainVcenterAdminPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto(`$BSTR)
		`$passwordCorrect=`$true
		`$vcenterUsername = ""administrator@vsphere.local""
		`$vcenterCredentials = New-Object System.Management.Automation.PSCredential -ArgumentList `$vcenterUsername, `$secureVcenterAdminPassword
	} else {
		Write-Host -ForegroundColor Red ""$passwordPrompt did not match. Please re-enter.""
	}
} while (`$passwordCorrect -ne `$true)")
	}

	#If AD integration is configured, prompts for AD user password  $vcenter_ad_join_user_password
	if ($vcenter_ad_join -eq "yes") {
		if ($domain_join_pass) {
			$secureVcenterDomainPassword = $domain_join_pass | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString -Key $secureStringKey 
			[Void]$scriptInputs.Add("`$secureVcenterDomainPassword = """ + $secureVcenterDomainPassword + """ | ConvertTo-SecureString -Key `$secureStringKey")
			[Void]$scriptInputs.Add("`$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$secureVcenterDomainPassword)")
			[Void]$scriptInputs.Add("`$plainVcenterDomainPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto(`$BSTR)")
		} else {
			[Void]$scriptInputs.Add($carriage_return)
			$passwordPrompt="AD '$domain_join_user' password"
			[Void]$scriptInputs.Add("do {
	`$passwordCorrect=`$false
	`$vcenterDomainPassword1 = Read-Host -Prompt ""Enter $passwordPrompt."" -AsSecureString
	`$vcenterDomainPassword2 = Read-Host -Prompt ""Re-enter $passwordPrompt."" -AsSecureString
	if ([Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$vcenterDomainPassword1)) -eq [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$vcenterDomainPassword2))) {
		`$secureVcenterDomainPassword = `$vcenterDomainPassword1
		`$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$secureVcenterDomainPassword)
		`$plainVcenterDomainPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto(`$BSTR)
		`$passwordCorrect=`$true
	} else {
		Write-Host -ForegroundColor Red ""$passwordPrompt did not match. Please re-enter.""
	}
} while (`$passwordCorrect -ne `$true)")
		}
	}
}

# If the vcenter deploy variable is set, output commands to deploy VCSA
if ($global_deploy_vcenter -eq "yes") {

	[Void]$vcenterVappDeployment.Add($section_break)
	[Void]$vcenterVappDeployment.Add("# Deploy VCSA")
	[Void]$vcenterVappDeployment.Add($section_break)
	[Void]$vcenterVappDeployment.Add($carriage_return)
	[Void]$vcenterVappDeployment.Add("`$objectFound=`$false")
	[Void]$vcenterVappDeployment.Add("do {")
	if ($vcenter_version -ge 6.0) {
		[Void]$vcenterVappDeployment.Add("`$vcenterPath = Read-Host -Prompt ""Please enter the path to the vCenter installer root directory.""
		if ( (Test-Path `$vcenterPath -PathType Container) ) { 
			`$objectFound=`$true
		} else {
			Write-Host -ForegroundColor Red ""Folder not found, please re-enter path."" 
		}")
	} else {
		[Void]$vcenterVappDeployment.Add("`$vcenterPath = Read-Host -Prompt ""Please enter path to the vCenter appliance OVA file.""
		if ( (Test-Path `$vcenterPath -PathType Leaf) ) { 
			`$objectFound=`$true
		} else {
			Write-Host -ForegroundColor Red ""OVA file not found, please re-enter path."" 
		}")
	}
	[Void]$vcenterVappDeployment.Add("} while (`$objectFound -ne `$true) ")
	[Void]$vcenterVappDeployment.Add($carriage_return)

	############### vApp Deployment Section for vSphere 6 and later
	if ($vcenter_version -ge 6.0) {
	}

	############### vApp Deployment Section for vSphere 5.5 and earlier
	if ($vcenter_version -le 5.5) {
		[Void]$vcenterVappDeployment.Add("foreach (`$ovftoolpath in `$ovftoolpaths) {
	if(test-path `$ovftoolpath) {
		`$ovftool = `$ovftoolpath
	}
}")	
		[Void]$vcenterVappDeployment.Add("if (!`$ovftool) {
	write-host -ForegroundColor red ""OVFtool not found in it's standard path.""
	write-host -ForegroundColor red ""Please download ovftool from this URL and install with default settings: http://www.vmware.com/support/developer/ovf/""
	exit
}")
		
		[Void]$vcenterVappDeployment.Add($carriage_return)
		[Void]$vcenterVappDeployment.Add("# Connect to ESXi Management host")	
		[Void]$vcenterVappDeployment.Add("do { 
	try {
		Connect-VIServer -Server $vcenter_esxi_host_fqdn -User root -Password `$plainEsxiRootPassword | Out-Null
	}
	catch {
		Write-Host -ForegroundColor Yellow ""Waiting for ESXi host to become available for logon.""
	}
	sleep 15
} until (`$global:DefaultVIServers.Count -eq 1)")
	
		#Check for maintenance mode
		[Void]$vcenterVappDeployment.Add($carriage_return)
		[Void]$vcenterVappDeployment.Add("`$ServerState = (Get-VMHost).ConnectionState
if (`$ServerState -eq ""Maintenance"") {
	Set-VMhost -State Connected | Out-Null
}")
	
		[Void]$vcenterVappDeployment.Add("Write-Host ""Deploying and configuring vCenter appliance""")
		[Void]$vcenterVappDeployment.Add("`$targetDatastore = Get-Datastore | Where { `$_.FreeSpaceGB -ge $vcsaMinDiskSpace } | Get-Random")
		[Void]$vcenterVappDeployment.Add("`$allDatastores = Get-Datastore")
		[Void]$vcenterVappDeployment.Add("if (!`$targetDatastore) {
	if (`$allDatastores.count -ge 1) {
		Write-Host -ForegroundColor Red ""No suitable datastore found with at least $vcsaMinDiskSpace GB of free space on ESXi host '$vcenter_esxi_host_fqdn'. Please check the ESXi host and retry.""
	} else {
		Write-Host -ForegroundColor Red ""No suitable datastore found on ESXi host '$vcenter_esxi_host_fqdn'. Please check the ESXi host and retry.""
	}
	break;
}")
		$vcenterShortName = $vcenter_fqdn.split(".")[0]
		#[Void]$vcenterVappDeployment.Add("`$ovftool_path = (Get-Location).Path + ""\tools\ovftool\ovftool.exe"" ")
		$ovfToolOptionsText = " --acceptAllEulas"
		$ovfToolOptionsText += " --noSSLVerify"
		$ovfToolOptionsText += " --skipManifestCheck"
		$ovfToolOptionsText += " --X:injectOvfEnv"
		$ovfToolOptionsText += " --network=""""$vcenter_port_group"""""
		$ovfToolOptionsText += " --datastore=""""`$targetDatastore"""""
		$ovfToolOptionsText += " --diskMode=thin"
		$ovfToolOptionsText += " --name=$vcenterShortName"
		$ovfToolOptionsText += " --prop:vami.hostname=$vcenter_fqdn"
		$ovfToolOptionsText += " --prop:vami.DNS.VMware_vCenter_Server_Appliance=$dns_server_ips_str"
		$ovfToolOptionsText += " --prop:vami.gateway.VMware_vCenter_Server_Appliance=$vcenter_gateway"
		$ovfToolOptionsText += " --prop:vami.ip0.VMware_vCenter_Server_Appliance=$vcenter_ip_address"
		$ovfToolOptionsText += " --prop:vami.netmask0.VMware_vCenter_Server_Appliance=$vcenter_subnet_mask"
		$ovfToolOptionsText += " """"`$vcenterPath"""""
		$ovfToolOptionsText += " vi://root:`$httpEsxiRootPassword@$vcenter_esxi_host_fqdn/"
		
		[Void]$vcenterVappDeployment.Add("`$p = Start-Process -FilePath ""`$ovftool"" -ArgumentList ""$ovfToolOptionsText"" -wait -WindowStyle Hidden -PassThru | Out-Null")
		[Void]$vcenterVappDeployment.Add("if ((`$p.HasExited) -and (`$p.ExitCode -ne 0)) {Write-Host -ForegroundColor Red ""VCSA Deployment Failed. Please check inputs and retry""; Break;}")
		[Void]$vcenterVappDeployment.Add("`$vcsaVm = Get-VM -Name $vcenterShortName")
		[Void]$vcenterVappDeployment.Add("Set-VM -VM `$vcsaVm -MemoryGB $vcenterApplianceRamGb -NumCpu $vcenterApplianceCpu -Notes ""vCenter Appliance deployed through Datalink Automation Engine for $customer_name_key"" -confirm:`$false | Out-Null")
		[Void]$vcenterVappDeployment.Add("Start-VM -VM `$vcsaVm -confirm:`$false -RunAsync | Out-Null")
		[Void]$vcenterVappDeployment.Add("do {
	`$toolsRunningStatus = (`$vcsaVm | Get-View).Guest.toolsRunningStatus
	Write-Host -ForegroundColor Yellow ""Waiting for `$vcsaVm to start.""
	sleep 30
} until (`$toolsRunningStatus -eq ""guestToolsRunning"")")
		[Void]$vcenterVappDeployment.Add($section_break)
		[Void]$vcenterVappDeployment.Add("# Configure VCSA")
		[Void]$vcenterVappDeployment.Add($section_break)
		[Void]$vcenterVappDeployment.Add("Sleep 15")
		$dns_server_ips_str_space = $dns_server_ips_str -replace ",", " "
		[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""echo only-once > /etc/vmware-vpx/ssl/allow_regeneration"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
		[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""/opt/vmware/share/vami/vami_set_hostname $vcenter_fqdn"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
		[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""/opt/vmware/share/vami/vami_set_network eth0 STATICV4 $vcenter_ip_address $vcenter_subnet_mask $vcenter_gateway"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
		[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""/opt/vmware/share/vami/vami_set_dns $dns_server_ips_str_space"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
		[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""/opt/vmware/share/vami/vami_set_timezone_cmd $timezone"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
		[Void]$vcenterVappDeployment.Add("Restart-VM -VM `$vcsaVm -confirm:`$false -RunAsync | Out-Null")
		[Void]$vcenterVappDeployment.Add("do {
	`$toolsRunningStatus = (Get-VM `$vcsaVm | Get-View).Guest.toolsRunningStatus
	Write-Host -ForegroundColor Yellow ""Waiting for `$vcsaVm to restart.""
	sleep 30
} until (`$toolsRunningStatus -eq ""guestToolsRunning"")")
		if ($vcenter_ad_join -eq "yes") {
			[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""/usr/sbin/vpxd_servicecfg ad write $domain_join_user `$plainVcenterDomainPassword $domain_name"" -GuestUser root -GuestPassword vmware | Out-Null)")
			[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText """"search $domain_name"" >> /etc/resolv.conf"" -GuestUser root -GuestPassword vmware | Out-Null)")
		} else {
			if (($ntp_server_names_str) -And ($ntp_server_names_str -notmatch "not_used")) {
				foreach ($ntp_server in $ntp_server_names) {
					[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""/usr/sbin/vpxd_servicecfg timesync write ntp $ntp_server"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
				}
			} else {
				[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""/usr/sbin/vpxd_servicecfg timesync write tools"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
			}
		}
		[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""/usr/sbin/vpxd_servicecfg eula accept"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
		[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""/usr/sbin/vpxd_servicecfg db write $vcenter_db_type"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
		[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""/usr/sbin/vpxd_servicecfg sso write $vcenter_sso_type `$plainVcenterAdminPassword"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
		[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""/usr/sbin/vpxd_servicecfg 'jvm-max-heap' 'write' '$vcenterApplianceTcserverMb' '$([int]$vcenterApplianceInvSvcGb*1024)' '$([int]$vcenterAppliancePDSorageGb*1024)'"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
		
		[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""/usr/sbin/vpxd_servicecfg service start"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
		[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""chage -M -1 -E -1 root"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
		#Not working yet
		#[Void]$vcenterVappDeployment.Add("`$scriptOutput = (Invoke-VMScript -VM `$vcsaVm -ScriptText ""echo `$plainVcenterAdminPassword | passwd --stdin"" -GuestUser root -GuestPassword vmware -ScriptType Bash)")
		[Void]$vcenterVappDeployment.Add($section_break)
		[Void]$vcenterVappDeployment.Add("# Restart VCSA")
		[Void]$vcenterVappDeployment.Add($section_break)
		[Void]$vcenterVappDeployment.Add("Restart-VM -VM `$vcsaVm -confirm:`$false -RunAsync | Out-Null")
		[Void]$vcenterVappDeployment.Add("do {
	`$toolsRunningStatus = (Get-VM `$vcsaVm | Get-View).Guest.toolsRunningStatus
	Write-Host -ForegroundColor Yellow ""Waiting for `$vcsaVm to restart.""
	sleep 30
} until (`$toolsRunningStatus -eq ""guestToolsRunning"")")
	}
}

# If the vcenter deploy variable is set, output commands to configure vCenter for this cluster
if ($global_configure_cluster -eq "yes") {
	[Void]$vcenterBaseConfig.Add($section_break)
	[Void]$vcenterBaseConfig.Add("# Configure vCenter")
	[Void]$vcenterBaseConfig.Add($section_break)
	#### Connect to vCenter
	[Void]$vcenterBaseConfig.Add($section_break)
	[Void]$vcenterBaseConfig.Add("# Connect to vCenter")
	[Void]$vcenterBaseConfig.Add($section_break)
	if (!$vcenter_ip_address) {
		[Void]$vcenterBaseConfig.Add("`$vcenter_ip_address = Read-Host -Prompt ""Please enter the IP address for the vCenter server.""")
	[Void]$vcenterBaseConfig.Add("do {
try {
	Connect-VIServer -Server `$vcenter_ip_address -Credential `$vcenterCredentials | Out-Null
}
catch {
	Write-Host -ForegroundColor Yellow ""Waiting for vCenter to become available for logon.""
}
sleep 60
} until (`$global:DefaultVIServers.Count -eq 1)")
	} else {
	[Void]$vcenterBaseConfig.Add("# Close all existing PowerCLI Connections")
	[Void]$vcenterBaseConfig.Add($section_break)
	[Void]$vcenterBaseConfig.Add("Disconnect-VIServer -Server * -Force -confirm:`$false | Out-Null")
	[Void]$vcenterBaseConfig.Add($section_break)
	[Void]$vcenterBaseConfig.Add("do {
try {
	Connect-VIServer -Server $vcenter_ip_address -Credential `$vcenterCredentials | Out-Null
}
catch {
	Write-Host -ForegroundColor Yellow ""Waiting for vCenter to become available for logon.""
}
sleep 60
} until (`$global:DefaultVIServers.Count -eq 1)")
	}
	
	if ($global_deploy_vcenter -eq "yes") {
		[Void]$vcenterBaseConfig.Add("sleep 60")
		#### Initial vCenter configuration
		# Import licence keys
		[Void]$vcenterBaseConfig.Add($section_break)
		[Void]$vcenterBaseConfig.Add("# Configure vCenter: Add Licence Keys")
		[Void]$vcenterBaseConfig.Add($section_break)
		if ($vcenter_licence_keys) {
			[Void]$vcenterBaseConfig.Add($carriage_return)
			[Void]$vcenterBaseConfig.Add("Write-Host ""Configuring vCenter licence keys""")
			[Void]$vcenterBaseConfig.Add("`$serviceInstance = Get-View ServiceInstance")
			[Void]$vcenterBaseConfig.Add("`$licenceManager = `$serviceInstance.Content.LicenseManager")
			[Void]$vcenterBaseConfig.Add("`$licenceManagerView = Get-View `$licenceManager")
			$licenceKeyArray = $vcenter_licence_keys.split(",")
			foreach($licenceKey in $licenceKeyArray) { 
				[Void]$vcenterBaseConfig.Add("`$licenceManagerView.AddLicense(""$licenceKey"",`$null) | Out-Null")
			}
		}

		#Setup vCenter SMTP & Monitoring
		[Void]$vcenterBaseConfig.Add($section_break)
		[Void]$vcenterBaseConfig.Add("# Configure vCenter: SMTP & Monitoring")
		[Void]$vcenterBaseConfig.Add($section_break)
		if (($smtp_server_name) -And ($smtp_server_name -notmatch "not_used") -And ($vcenter_smtp_from_addr) -And ($vcenter_smtp_from_addr -notmatch "not_used")) {
			[Void]$vcenterBaseConfig.Add($carriage_return)
			[Void]$vcenterBaseConfig.Add("Write-Host ""Configuring vCenter SNMP and SMTP""")
			[Void]$vcenterBaseConfig.Add("Get-AdvancedSetting -entity `$defaultVIServers[0] -name 'mail.smtp.server' | Set-AdvancedSetting -value ""$smtp_server_name"" -confirm:`$false | Out-Null")
			[Void]$vcenterBaseConfig.Add("Get-AdvancedSetting -entity `$defaultVIServers[0] -name 'mail.sender' | Set-AdvancedSetting -value ""$vcenter_smtp_from_addr""-confirm:`$false  | Out-Null")
			[Void]$vcenterBaseConfig.Add("Get-AdvancedSetting -entity `$defaultVIServers[0] -name 'mail.smtp.port' | Set-AdvancedSetting -value 25 -confirm:`$false | Out-Null")
		}
		if (($snmp_server_name) -And ($smtp_server_name -notmatch "not_used")) {
			[Void]$vcenterBaseConfig.Add("Get-AdvancedSetting -entity `$defaultVIServers[0] -name 'snmp.reciever.1.name' | Set-AdvancedSetting -value ""$snmp_server_name"" -confirm:`$false | Out-Null")
		}
		if (($snmp_community_string) -And ($snmp_community_string -notmatch "not_used")) {
			[Void]$vcenterBaseConfig.Add("Get-AdvancedSetting -entity `$defaultVIServers[0] -name 'snmp.reciever.1.community' | Set-AdvancedSetting -value ""$snmp_community_string"" -confirm:`$false | Out-Null")
		}

		#Setup vCenter Monitoring Levels
		[Void]$vcenterBaseConfig.Add($section_break)
		[Void]$vcenterBaseConfig.Add("# Configure vCenter: Monitoring Levels")
		[Void]$vcenterBaseConfig.Add($section_break)
		[Void]$vcenterBaseConfig.Add("Write-Host ""Configuring vCenter statistics levels""")
		[Void]$vcenterBaseConfig.Add("`$perfcounter = Get-View (Get-View ServiceInstance).Content.PerfManager")
		[Void]$vcenterBaseConfig.Add("`$perfcounter.HistoricalInterval | % { 
		`$newinterval = new-object VMware.Vim.PerfInterval
		`$newinterval.key = `$_.Key
		`$newinterval.Name = `$_.Name
		`$newinterval.SamplingPeriod = `$_.SamplingPeriod
		`$newinterval.Length = `$_.Length
		`$newinterval.Enabled = `$true
		switch (`$_.Key) {
			1 {`$newinterval.Level = $vcenterMonLevel1; break;}
			2 {`$newinterval.Level = $vcenterMonLevel2; break;}
			3 {`$newinterval.Level = $vcenterMonLevel3; break;}
			4 {`$newinterval.Level = $vcenterMonLevel4; break;}
		}
		`$perfcounter.UpdatePerfInterval(`$newinterval)
	}")

		if ($vcenterTaskRetentionEnabled) {
			[Void]$vcenterBaseConfig.Add($section_break)
			[Void]$vcenterBaseConfig.Add("# Configure vCenter: Task Retention")
			[Void]$vcenterBaseConfig.Add($section_break)
			[Void]$vcenterBaseConfig.Add("Write-Host ""Configuring vCenter task retention""")
			[Void]$vcenterBaseConfig.Add("Get-AdvancedSetting -entity `$defaultVIServers[0] -name 'task.maxAge' | Set-AdvancedSetting -value $vcenterTaskRetention")
			[Void]$vcenterBaseConfig.Add("Get-AdvancedSetting -entity `$defaultVIServers[0] -name 'task.maxAgeEnabled' | Set-AdvancedSetting -value `$true")
		}

		if ($vcenterEventRetentionEnabled) {
			[Void]$vcenterBaseConfig.Add($section_break)
			[Void]$vcenterBaseConfig.Add("# Configure vCenter: Event Retention")
			[Void]$vcenterBaseConfig.Add($section_break)
			[Void]$vcenterBaseConfig.Add("Write-Host ""Configuring vCenter event Retention""")
			[Void]$vcenterBaseConfig.Add("Get-AdvancedSetting -entity `$defaultVIServers[0] -name 'event.maxAge' | Set-AdvancedSetting -value $vcenterEvent")
			[Void]$vcenterBaseConfig.Add("Get-AdvancedSetting -entity `$defaultVIServers[0] -name 'event.maxAgeEnabled' | Set-AdvancedSetting -value `$true")
		}
	}
	#### Create Datacenters
	[Void]$vcenterBaseConfig.Add($section_break)
	[Void]$vcenterBaseConfig.Add("# Configure vCenter: Create Datacenter")
	[Void]$vcenterBaseConfig.Add($section_break)
	[Void]$vcenterBaseConfig.Add("Write-Host ""Creating vCenter Datacenters""")
	[Void]$vcenterBaseConfig.Add("if (-Not (`$NewDatacenter = Get-Datacenter ""$datacenter_name"" -ErrorAction SilentlyContinue)) {
	New-DataCenter -Location (Get-Folder -NoRecursion) -Name ""$datacenter_name"" | Out-Null
}")

	#### Create Clusters
	[Void]$vcenterCluster.Add($section_break)
	[Void]$vcenterCluster.Add("# Configure vCenter: Create Clusters")
	[Void]$vcenterCluster.Add($section_break)
	[Void]$vcenterCluster.Add("Write-Host ""Creating vCenter clusters""")
	
	$drs_enabled = $cluster.drs_enabled
	$drs_mode = $cluster.drs_mode
	$ha_enabled = $cluster.ha_enabled
	$ha_admission_control_enabled = $cluster.ha_admission_control_enabled
	$ha_admission_control_policy = $cluster.ha_admission_control_policy
	$ha_admission_control_num_host_failures = $cluster.ha_admission_control_num_host_failures
	$ha_admission_control_percent_cpu = $cluster.ha_admission_control_percent_cpu
	$ha_admission_control_percent_memory = $cluster.ha_admission_control_percent_memory
	$evc_mode = $cluster.evc_mode
	$vsan_enabled = $cluster.vsan_enabled
	$vsan_disk_claim_mode = $cluster.vsan_disk_claim_mode
	
	[Void]$vcenterCluster.Add($carriage_return)
	$clusterConfigString = "New-Cluster -Name ""$global_esxi_cluster_name"""
	$clusterConfigString += " -Location (Get-Datacenter -Name ""$datacenter_name"")"
	if ($drs_enabled -eq "yes"){$clusterConfigString += " -DRSEnabled"}
	switch ($drs_mode) {
		"fully-automated" {$clusterConfigString += " -DRSAutomationLevel FullyAutomated"; break}
		"partially-automated" {$clusterConfigString += " -DRSAutomationLevel PartiallyAutomated"; break}
		"manual" {$clusterConfigString += " -DRSAutomationLevel Manual"; break}
		default {$clusterConfigString += " -DRSAutomationLevel FullyAutomated"; break}
	}
	if ($ha_enabled -eq "yes"){$clusterConfigString += " -HAEnabled"}
	if ($ha_admission_control_enabled -eq "yes") {$clusterConfigString = $clusterConfigString + " -HAAdmissionControlEnabled"}
	if ($ha_admission_control_policy -eq "number-of-hosts") {$clusterConfigString = $clusterConfigString + " -HAFailoverLevel $ha_admission_control_num_host_failures"}
	switch ($evc_mode) {
		"Disabled" {break}
		"Intel Merom Generation" {$clusterConfigString = $clusterConfigString + " -EVCMode 'intel-merom'"; break}
		"Intel Penryn Generation" {$clusterConfigString = $clusterConfigString + " -EVCMode 'intel-penryn'"; break}
		"Intel Nehalem Generation" {$clusterConfigString = $clusterConfigString + " -EVCMode 'intel-nehalem'"; break}
		"Intel Westmere Generation" {$clusterConfigString = $clusterConfigString + " -EVCMode 'intel-westmere'"; break}
		"Intel Sandy Bridge Generation" {$clusterConfigString = $clusterConfigString + " -EVCMode 'intel-sandybridge'"; break}
		"Intel Ivy Bridge Generation" {$clusterConfigString = $clusterConfigString + " -EVCMode 'intel-ivybridge'"; break}
		"AMD Opteron Generation 1" {$clusterConfigString = $clusterConfigString + " -EVCMode 'amd-rev-e'"; break}
		"AMD Opteron Generation 2" {$clusterConfigString = $clusterConfigString + " -EVCMode 'amd-rev-f'"; break}
		"AMD Opteron Generation 3" {$clusterConfigString = $clusterConfigString + " -EVCMode 'amd-greyhound'"; break}
		"AMD Opteron Generation 4" {$clusterConfigString = $clusterConfigString + " -EVCMode 'amd-bulldozer'"; break}
		"AMD Opteron Piledriver Generation" {$clusterConfigString = $clusterConfigString + " -EVCMode 'amd-piledriver'"; break}
		default {}
	}
	if ($vsan_enabled -eq "yes") {
		$clusterConfigString += " -VsanEnabled"
		switch ($vsan_disk_claim_mode) {
			"automatic" {$clusterConfigString += " -VsanDiskClaimMode Automatic"; break}
			"manual" {$clusterConfigString += " -VsanDiskClaimMode Manual"; break}
			Default {}
		}
	}
	$clusterConfigString = $clusterConfigString + " -VMSwapfilePolicy ""WithVM"""
	[Void]$vcenterCluster.Add($clusterConfigString + " | Out-Null")
	
	if ($ha_admission_control_policy -eq "percentage-of-cluster-resources") {
		[Void]$vcenterCluster.Add("`$spec = New-Object VMware.Vim.ClusterConfigSpecEx
`$spec.dasConfig = New-Object VMware.Vim.ClusterDasConfigInfo
`$spec.dasConfig.admissionControlPolicy = New-Object VMware.Vim.ClusterFailoverResourcesAdmissionControlPolicy
`$spec.dasConfig.admissionControlPolicy.cpuFailoverResourcesPercent = $ha_admission_control_percent_cpu
`$spec.dasConfig.admissionControlPolicy.memoryFailoverResourcesPercent = $ha_admission_control_percent_memory
`$clusterView = Get-View (Get-Cluster ""$global_esxi_cluster_name"")
`$clusterView.ReconfigureComputeResource_Task(`$spec, `$true) | Out-Null")
	}

	#### Add Hosts to Clusters
	[Void]$vcenterCluster.Add($section_break)
	[Void]$vcenterCluster.Add("# Configure vCenter: Add Hosts to Clusters")
	[Void]$vcenterCluster.Add($section_break)
	[Void]$vcenterCluster.Add("Write-Host ""Adding ESXi hosts to vCenter clusters""")
	foreach ($clusterHost in $clusterHosts) {
		$esxi_hostname = $clusterHost.esxi_hostname_fqdn
		$licence_key = $clusterHost.esxi_licence_key
		#$clusterHostConfigString = "Add-VMHost -Name $esxi_hostname -Location (Get-Cluster -Name ""$global_esxi_cluster_name"") -Credential `$esxiCredentials -confirm:`$false -Force"
		[Void]$vcenterCluster.Add("Add-VMHost -Name $esxi_hostname -Location (Get-Cluster -Name ""$global_esxi_cluster_name"") -Credential `$esxiCredentials -confirm:`$false -Force | Out-Null")
		if ($licence_key) { [Void]$vcenterCluster.Add("Set-VMHost -VMHost $esxi_hostname -LicenseKey ""$licence_key"" -confirm:`$false" + " | Out-Null") }
	}
	
	#### Check hosts and disable maintainance mode is enabled
	[Void]$vcenterCluster.Add($section_break)
	[Void]$vcenterCluster.Add("# Configure vCenter: Take hosts out of maintenance mode")
	[Void]$vcenterCluster.Add($section_break)
	[Void]$vcenterNetworking.Add("`$clusterHosts = Get-Cluster -Name ""$global_esxi_cluster_name"" | Get-VMHost")
	[Void]$vcenterNetworking.Add("foreach (`$clusterHost in `$clusterHosts) {
	`$connectionState = (Get-VMHost `$clusterHost).ConnectionState
	if (`$connectionState -eq ""Maintenance"") {
		Set-VMhost `$clusterHost -State Connected | Out-Null
	}
}")
	
	#### Create dvSwitches
	if ($dvSwitches) {
		[Void]$vcenterNetworking.Add($section_break)
		[Void]$vcenterNetworking.Add("# Configure vCenter: Create dvSwitches")
		[Void]$vcenterNetworking.Add($section_break)
		[Void]$vcenterNetworking.Add("Write-Host ""Creating vCenter dvSwitches""")
		$processedDvswitches = @()
		$dvSwitchProcessed = $false
		foreach ($dvswitch in $dvSwitches) {
			$dvswitch_name = $dvswitch.dvswitch_name
			$dv_switch_description = $dvswitch.dv_switch_description
			$dvswitch_uplinks = $dvswitch.dvswitch_uplinks
			$dvswitch_link_discovery_protocol = $dvswitch.dvswitch_link_discovery_protocol
			$dvswitch_link_discovery_operation = $dvswitch.dvswitch_link_discovery_operation
			$dvswitch_mtu = $dvswitch.dvswitch_mtu
			$dvswitch_max_ports = $dvswitch.dvswitch_max_ports
			
			[Void]$vcenterNetworking.Add("if (-Not (`$NewVswitch = Get-VDSwitch ""$dvswitch_name"" -ErrorAction SilentlyContinue)) {")
			
			$dvSwitchConfigString = "	New-VDSwitch -Name ""$dvswitch_name"""
			$dvSwitchConfigString = $dvSwitchConfigString + " -Location (Get-Datacenter -Name ""$datacenter_name"")"
			if ($dv_switch_description) {$dvSwitchConfigString = $dvSwitchConfigString + " -Notes ""$dv_switch_description"""}
			switch ($dvswitch_link_discovery_protocol) {
				"cdp" { $dvSwitchConfigString = $dvSwitchConfigString + " -LinkDiscoveryProtocol CDP"; break }
				"lldp" { $dvSwitchConfigString = $dvSwitchConfigString + " -LinkDiscoveryProtocol LLDP"; break }
				default { }
			}
			switch ($dvswitch_link_discovery_operation) {
				"listen" { $dvSwitchConfigString = $dvSwitchConfigString + " -LinkDiscoveryProtocolOperation Listen"; break }
				"advertise" { $dvSwitchConfigString = $dvSwitchConfigString + " -LinkDiscoveryProtocolOperation Advertise"; break }
				"both" { $dvSwitchConfigString = $dvSwitchConfigString + " -LinkDiscoveryProtocolOperation Both"; break }
				default { }
			}
			if ($dvswitch_max_ports) {$dvSwitchConfigString = $dvSwitchConfigString + " -MaxPorts $dvswitch_max_ports"}
			$vdswitchUplinkArray = $dvswitch_uplinks.Split(",")
			$dvSwitchConfigString = $dvSwitchConfigString + " -NumUplinkPorts " + $vdswitchUplinkArray.length
			[Void]$vcenterNetworking.Add($dvSwitchConfigString + " -confirm:`$false | Out-Null")
			
			[Void]$vcenterNetworking.Add("}")
		}
	}
	
	#### Create dvPorts on dvSwitches
	if ($dvSwitchPortGroups) {
		[Void]$vcenterNetworking.Add($section_break)
		[Void]$vcenterNetworking.Add("# Configure vCenter: Create dvSwitch Port Groups")
		[Void]$vcenterNetworking.Add($section_break)
		[Void]$vcenterNetworking.Add("Write-Host ""Creating vCenter dvSwitch port groups""")
		foreach ($dvswitchPortgroup in $dvSwitchPortGroups) {
			$dvswitch_name = $dvswitchPortgroup.dvswitch_name
			$dvportgroup_name = $dvswitchPortgroup.dvportgroup_name
			$dvportgroup_numports = $dvswitchPortgroup.dvportgroup_numports
			$dvportgroup_port_binding = $dvswitchPortgroup.dvportgroup_port_binding
			$dvportgroup_vlan = $dvswitchPortgroup.dvportgroup_vlan
			
			[Void]$vcenterNetworking.Add("if (-Not (`$NewVswitchPg = Get-VDPortgroup -Name ""$dvportgroup_name"" -ErrorAction SilentlyContinue)) {")
			[Void]$vcenterNetworking.Add("	Get-VDSwitch -Name ""$dvswitch_name"" | New-VDPortgroup -Name ""$dvportgroup_name"" -NumPorts $dvportgroup_numports -VLanId $dvportgroup_vlan -RunAsync -confirm:`$false | Out-Null")
			[Void]$vcenterNetworking.Add("}")
		}
	}
	
	#### Assign dvSwitches to Hosts
	if ($dvSwitches) {
		[Void]$vcenterNetworking.Add($section_break)
		[Void]$vcenterNetworking.Add("# Configure vCenter: Add dvSwitches to Hosts")
		[Void]$vcenterNetworking.Add($section_break)
		[Void]$vcenterNetworking.Add("Write-Host ""Connecting vCenter dvSwitches to ESXi hosts""")
		foreach ($dvSwitch in $dvSwitches) {
			$dvswitch_name = $dvSwitch.dvswitch_name
			$dvswitch_uplinks = $dvSwitch.dvswitch_uplinks
			$dvswitch_mtu = $dvSwitch.dvswitch_mtu
			[Void]$vcenterNetworking.Add("`$dvswitchHosts = Get-Cluster -Name ""$global_esxi_cluster_name"" | Get-VMHost")
			[Void]$vcenterNetworking.Add("if (!`$dvswitchHosts) {")
			[Void]$vcenterNetworking.Add("`$dvswitch = Get-VDSwitch -Name ""$dvswitch_name""")
			if ($dvswitch_uplinks) {
				$vdswitchUplinkArray = $dvswitch_uplinks.Split(",")
				[Void]$vcenterNetworking.Add("foreach (`$dvswitchHost in `$dvswitchHosts) {")
				[Void]$vcenterNetworking.Add("	Get-VDSwitch -Name `$dvswitch | Add-VDSwitchVMHost -VMHost `$dvswitchHost")
				foreach ($vdswitchUplink in $vdswitchUplinkArray) {
					[Void]$vcenterNetworking.Add("	`$vmhostNetworkAdapter = `$dvswitchHost | Get-VMHostNetworkAdapter -Physical -Name $vdswitchUplink")
					[Void]$vcenterNetworking.Add("	`$dvswitch | Add-VDSwitchPhysicalNetworkAdapter -VMHostPhysicalNic `$vmhostNetworkAdapter -confirm:$false | Out-Null")
				}
				[Void]$vcenterNetworking.Add("}")
			}
			[Void]$vcenterNetworking.Add("}")
			[Void]$vcenterNetworking.Add($carriage_return)
		}
	}
	
	#### Create Folders
	if ($folders) {
		[Void]$vcenterFolders.Add($section_break)
		[Void]$vcenterFolders.Add("# Configure vCenter: Create Folders")
		[Void]$vcenterFolders.Add($section_break)
		[Void]$vcenterFolders.Add("Write-Host ""Creating vCenter folders""")
		foreach ($folder in $folders) {
			#$datacenter_name = $folder.datacenter_name
			$parent = $folder.parent
			$folder_name = $folder.folder_name
			
			if ($parent) {
				[Void]$vcenterFolders.Add("New-Folder -Name ""$folder_name"" -Location (Get-Folder -name ""$parent"") | Out-Null")
			} else {
				[Void]$vcenterFolders.Add("(get-view (get-view -viewtype datacenter -filter @{""name""=""$datacenter_name""}).vmfolder).createfolder(""$folder_name"") | Out-Null")
			}
		}
	}
	
	#### Create Permissions
	[Void]$vcenterPermissions.Add($section_break)
	[Void]$vcenterPermissions.Add("# Configure vCenter: Configure Permissions")
	[Void]$vcenterPermissions.Add($section_break)

	#### Configure Alarms
	if ($alarms) {
		[Void]$vcenterAlarms.Add($section_break)
		[Void]$vcenterAlarms.Add("# Configure vCenter: Create Alarms")
		[Void]$vcenterAlarms.Add($section_break)
		[Void]$vcenterAlarms.Add($carriage_return)
		[Void]$vcenterAlarms.Add("Write-Host ""Configuring vCenter alarms""")
		#Clear all existing alarms
		[Void]$vcenterAlarms.Add("Get-AlarmDefinition | Get-AlarmAction | Remove-AlarmAction -Confirm:`$false | Out-Null")
		foreach ($alarm in $alarms) {
			$alarm_name = $alarm.alarm_name
			$alarm_action = $alarm.alarm_action
			$alarm_action_email_addresses = $alarm.alarm_action_email_addresses
			$alarm_notify_green_to_yellow = $alarm.alarm_notify_green_to_yellow
			$alarm_notify_yellow_to_red = $alarm.alarm_notify_yellow_to_red
			$alarm_notify_red_to_yellow = $alarm.alarm_notify_red_to_yellow
			$alarm_notify_yellow_to_green = $alarm.alarm_notify_yellow_to_green
			$alarm_repeat = $alarm.alarm_repeat
			$alarm_repeat_interval = $alarm.alarm_repeat_interval
			
			if (($alarm_notify_green_to_yellow -eq "yes") -Or ($alarm_notify_yellow_to_red -eq "yes") -Or ($alarm_notify_red_to_yellow -eq "yes") -Or ($alarm_notify_yellow_to_green -eq "yes")) {

				switch ($alarm_action) {
					"SendEmail" {
						$alarmActionEmailAddressArray = $alarm_action_email_addresses.Split(",")
						foreach($alarmActionEmailAddress in $alarmActionEmailAddressArray) {
							[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | New-AlarmAction -Email -To ""$alarmActionEmailAddress"" | Out-Null")
						}
						
						if ($alarm_repeat -eq "yes") {
							if ($alarm_notify_green_to_yellow -eq "yes") {
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | New-AlarmActionTrigger -StartStatus 'Green' -EndStatus 'Yellow' -Repeat | Out-Null")
							}
							if ($alarm_notify_red_to_yellow -eq "yes") {
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | New-AlarmActionTrigger -StartStatus 'Red' -EndStatus 'Yellow' -Repeat | Out-Null")
							}
							if ($alarm_notify_yellow_to_green -eq "yes") {
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | New-AlarmActionTrigger -StartStatus 'Yellow' -EndStatus 'Green' -Repeat | Out-Null")
							}
							if ($alarm_notify_yellow_to_red -ne "yes") {
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | Get-AlarmActionTrigger | where {`$_.StartStatus -eq ""Yellow"" -and `$_.EndStatus -eq ""Red""} | Remove-AlarmActionTrigger -Confirm:`$false | Out-Null")
							}
							#If the default is not the only one set, delete the default and recreate it
							if (($alarm_notify_yellow_to_red -eq "yes") -And (($alarm_notify_green_to_yellow -eq "yes") -Or ($alarm_notify_red_to_yellow -eq "yes") -Or ($alarm_notify_yellow_to_green -eq "yes"))) {
								#Remove default alarm
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | Get-AlarmActionTrigger | where {`$_.StartStatus -eq ""Yellow"" -and `$_.EndStatus -eq ""Red""} | Remove-AlarmActionTrigger -Confirm:`$false | Out-Null")
								#Add alarm with repeat set
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | New-AlarmActionTrigger -StartStatus 'Yellow' -EndStatus 'Red' -Repeat | Out-Null")
							}
							#If the default is the only one set, add a temp alarm to be able to delete the default
							if (($alarm_notify_yellow_to_red -eq "yes") -And ($alarm_notify_green_to_yellow -eq "no") -And ($alarm_notify_red_to_yellow -eq "no") -And ($alarm_notify_yellow_to_green -eq "no")) {
								#Add temp alarm
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | New-AlarmActionTrigger -StartStatus 'Green' -EndStatus 'Yellow' | Out-Null")
								#Remove default alarm
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | Get-AlarmActionTrigger | where {`$_.StartStatus -eq ""Yellow"" -and `$_.EndStatus -eq ""Red""} | Remove-AlarmActionTrigger -Confirm:`$false | Out-Null")
								#Add back default alarm with repeat set
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | New-AlarmActionTrigger -StartStatus 'Yellow' -EndStatus 'Red' -Repeat | Out-Null")
								#Remove temp alarm
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | Get-AlarmActionTrigger | where {`$_.StartStatus -eq ""Green"" -and `$_.EndStatus -eq ""Yellow""} | Remove-AlarmActionTrigger -Confirm:`$false | Out-Null")
							}
							[Void]$vcenterAlarms.Add("Set-AlarmDefinition ""$alarm_name"" -ActionRepeatMinutes $alarm_repeat_interval | Out-Null")
						} else {
							if ($alarm_notify_green_to_yellow -eq "yes") {
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | New-AlarmActionTrigger -StartStatus 'Green' -EndStatus 'Yellow' | Out-Null")
							}
							if ($alarm_notify_red_to_yellow -eq "yes") {
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | New-AlarmActionTrigger -StartStatus 'Red' -EndStatus 'Yellow' | Out-Null")
							}
							if ($alarm_notify_yellow_to_green -eq "yes") {
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | New-AlarmActionTrigger -StartStatus 'Yellow' -EndStatus 'Green' | Out-Null")
							}
							#Default alarm is created automatically so this only needs to remove it if anything else is set 
							if ($alarm_notify_yellow_to_red -ne "yes") {
								[Void]$vcenterAlarms.Add("Get-AlarmDefinition ""$alarm_name"" | Get-AlarmAction -ActionType SendEmail | Get-AlarmActionTrigger | where {`$_.StartStatus -eq ""Yellow"" -and `$_.EndStatus -eq ""Red""} | Remove-AlarmActionTrigger -Confirm:`$false | Out-Null")
							}
						} #End If alarm_repeat
					}
				} #End Switch
			} #End If
		} #End Foreach
	}
	
	#Not working correctly, to be added in future release
	#### Implement vSphere Hardening
	#if ($global_vcenter_apply_hardening -eq "yes") {
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("# Configure vCenter: Apply vSphere Hardening")
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add($carriage_return)
	#	[Void]$vcenterHardening.Add("Write-Host ""Applying vCenter hardening""")
    #
	#	if (($ntp_server_names_str) -And ($ntp_server_names_str -notmatch "not_used")) {
	#		[Void]$vcenterHardening.Add($section_break)
	#		[Void]$vcenterHardening.Add("# Set the NTP Settings for all hosts")
	#		[Void]$vcenterHardening.Add($section_break)
	#		foreach ($ntp_server in $ntp_server_names) {
	#			[Void]$vcenterHardening.Add("Get-VMHost | Add-VmHostNtpServer $ntp_server | Out-Null")
	#		}
	#	}
	#
	## Set vCenter Advanced Parameters
	#[Void]$vcenterHardening.Add("Get-AdvancedSetting -entity `$defaultVIServers[0] -name 'config.enableHttpDatastoreAccess' | Set-AdvancedSetting -value `$false -confirm:`$false | Out-Null")
	#[Void]$vcenterHardening.Add("Get-AdvancedSetting -entity `$defaultVIServers[0] -name 'config.vpxd.enableDebugBrowse' | Set-AdvancedSetting -value `$false -confirm:`$false | Out-Null")
	#
	## Set vSwitch Security
	#[Void]$vcenterHardening.Add("Get-VirtualSwitch -Standard | Get-SecurityPolicy | Set-SecurityPolicy -MacChanges `$false -ForgedTransmits `$false -AllowPromiscuous `$false -confirm:`$false | Out-Null")
	#
	#	if($snmp_community_string) {
	#		[Void]$vcenterHardening.Add($section_break)
	#		[Void]$vcenterHardening.Add("# Update the host SNMP Configuration (single host connection required)")
	#		[Void]$vcenterHardening.Add($section_break)	
	#		[Void]$vcenterHardening.Add("Get-VmHostSNMP | Set-VMHostSNMP -Enabled:`$true -ReadOnlyCommunity '$snmp_community_string' | Out-Null")
	#	}	
    #
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("# Set ESXi Shell to start manually rather than automatic for all hosts")
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("Get-VMHost | Get-VMHostService | Where { `$_.key -eq ""TSM"" } | Set-VMHostService -Policy Off | Out-Null")
	#	
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("# Set SSH to start manually rather than automatic for all hosts")
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("Get-VMHost | Get-VMHostService | Where { `$_.key -eq ""TSM-SSH"" } | Set-VMHostService -Policy Off | Out-Null")
    #
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("# Join the ESXI Host to the Domain")
	#	[Void]$vcenterHardening.Add($section_break)
	#	if ($vcenter_ad_join -eq "yes") {
	#		[Void]$vcenterVappDeployment.Add("`$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR(`$secureVcenterDomainPassword)")
	#		[Void]$vcenterVappDeployment.Add("`$plainVcenterDomainPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto(`$BSTR)")
	#		foreach ($clusterHost in $clusterHosts) {
	#			$esxi_hostname = $clusterHost.esxi_hostname_fqdn
	#			[Void]$vcenterHardening.Add("Get-VMHost -Name $esxi_hostname | Get-VMHostAuthentication | Set-VMHostAuthentication -Domain $vcenter_ad_join_domain -User $vcenter_ad_join_user -Password `$plainVcenterDomainPassword -JoinDomain | Out-Null")
	#		}
	#	}
	#	
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("# Set host remote coredump location")
	#	[Void]$vcenterHardening.Add($section_break)
	#	foreach ($clusterHost in $clusterHosts) {
	#		$esxi_hostname = $clusterHost.esxi_hostname_fqdn
	#		[Void]$vcenterHardening.Add("`$esxcli = Get-EsxCli -VMHost $esxi_hostname")
	#		[Void]$vcenterHardening.Add("`$esxcli.system.coredump.network.set(`$null, ""vmk0"", ""$vcenter_ip_address"", ""6500"")")
	#		[Void]$vcenterHardening.Add("`$esxcli.system.coredump.network.set(`$true)")
	#	}
    #
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("# Set Syslog.global.logHost for each host")
	#	[Void]$vcenterHardening.Add($section_break)
	#	if ($syslog_server_names_str) {
	#		[Void]$vcenterHardening.Add("Get-AdvancedSetting -Entity (Get-Cluster -Name ""$global_esxi_cluster_name"") -Name Syslog.global.logHost | Set-AdvancedSetting -Value ""$syslog_server_names_str"" | Out-Null")
	#	}
	#	
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("# Set Remove UserVars.ESXiShellInteractiveTimeOut to $esxiHostDefaultShellTimeout on all hosts")
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("Get-AdvancedSetting -Entity (Get-Cluster -Name ""$global_esxi_cluster_name"") -Name UserVars.ESXiShellInteractiveTimeOut | Set-AdvancedSetting -Value $esxiHostDefaultShellTimeout | Out-Null")
    #
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("# Set Remove UserVars.ESXiShellTimeOut to $esxiHostDefaultSshTimeout on all hosts")
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("Get-VMHost | Foreach { Set-VMHostAdvancedConfiguration -VMHost `$_ -Name UserVars.ESXiShellTimeOut -Value $esxiHostDefaultSshTimeout } | Out-Null")
    #
	#	[Void]$vcenterHardening.Add($section_break)
	#	[Void]$vcenterHardening.Add("# Set the Software AcceptanceLevel for each host")
	#	[Void]$vcenterHardening.Add($section_break)
	#	foreach ($clusterHost in $clusterHosts) {
	#		$esxi_hostname = $clusterHost.esxi_hostname_fqdn
	#		[Void]$vcenterHardening.Add("`$esxcli = Get-EsxCli -VMHost $esxi_hostname")
	#		[Void]$vcenterHardening.Add("`$esxcli.software.acceptance.Set(""VMwareCertified"")")
	#	}
	#}

	[Void]$vcenterDisconnect.Add($section_break)
	[Void]$vcenterDisconnect.Add("# Disconnect from vCenter")
	[Void]$vcenterDisconnect.Add($section_break)
	[Void]$vcenterDisconnect.Add("Disconnect-VIServer -Server $vcenter_ip_address -confirm:`$false | Out-Null")
}

# Combine the sections to form the output of the powershell script
$outputScript = $scriptOptions + 
	$scriptInputs +
	$vcenterVappDeployment + 
	$vcenterBaseConfig + 
	$vcenterCluster + 
	$vcenterNetworking + 
	$vcenterFolders + 
	$vcenterPermissions +
	$vcenterAlarms +
	$vcenterHardening +
	$vcenterDisconnect

# output the file
$filename = "vcenter-config-$global_build_id"
$outputScript | Out-File "$vcenter_cmds_path\$filename.ps1" -Encoding ASCII