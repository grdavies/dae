Param(
  [Parameter(Mandatory=$True,Position=1)]
  [string]$datasource_name,
  [Parameter(Mandatory=$True,Position=2)]
  [string]$build_cmds,
  [Parameter(Mandatory=$True,Position=3)]
  [string]$build_docs,
  [Parameter(Mandatory=$True,Position=4)]
  [string]$build_id
  
)

$this_script 	= ($MyInvocation.MyCommand).Name

#region Header

$wwns_data		= $svos_data.get_Item("svos_wwns")
$ldevs_data		= $svos_data.get_Item("svos_ldevs")

$as_built_build_id		= New-Object System.Collections.ArrayList
$as_built_customer		= New-Object System.Collections.ArrayList
$as_built_wwns			= New-Object System.Collections.ArrayList
$as_built_ldevs			= New-Object System.Collections.ArrayList

$customer_name_key		= ((($global_customer | select customer_name).customer_name).Replace(" ","")).ToLower()
$svos_array_name		= $global_svos_array_name

$datasource_basename	= Split-Path -Path $datasource_name -Leaf
$cmd_output_name 		= $customer_name_key + "_" + $svos_array_name + "_svos_build_cmds.txt"
$cmd_output_file 		= Join-Path -Path $svos_cmds_path -ChildPath $cmd_output_name
$table_defs_name 		= "svos_table_defs_" + $datasource_name + ".txt"
$table_defs_file 		= Join-Path -Path $cdot_reports_path -ChildPath $table_defs_name

#endregion

#region Get global data
$global_customer | %{			
	$customer_name			= $_.customer_name
	$contact_name			= $_.contact_name
	$contact_email			= $_.contact_email
	$contact_phone			= $_.contact_phone
	}
$global_location | %{
	$customer_address		= $_.customer_address
	$system_install_address	= $_.system_install_address
	$rma_address			= $_.rma_address
	$rma_attn_to_name		= $_.rma_attn_to_name
	}
$global_datacenter | %{
	$domain_name			= $_.domain_name
	$dns_server_ips_str		= $_.dns_server_ips
	$ntp_server_names_str	= $_.ntp_server_names
	$smtp_server_name		= $_.smtp_server_name
	$snmp_server_name		= $_.snmp_server_name
	$snmp_community_string	= $_.snmp_community_string
	$timezone				= $_.timezone
	$default_admin_user		= $_.default_admin_user
	$default_admin_pass		= $_.default_admin_pass
	
	if ($dns_server_ips_str -match ",")
		{
		$dns_server_ips		= $dns_server_ips_str.Split(",")
		}
	else
		{
		$dns_server_ips		= @($dns_server_ips_str)
		}
	if ($ntp_server_names_str -match ",")
		{
		$ntp_server_names	= $ntp_server_names_str.Split(",")
		}
	else
		{
		$ntp_server_names	= @($ntp_server_names_str)
		}
	}
	
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Contact Name"
$as_built_customer_obj.customer_value	= $contact_name
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Contact Email"
$as_built_customer_obj.customer_value	= $contact_email
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Contact Phone"
$as_built_customer_obj.customer_value	= $contact_phone
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Customer Address"
$as_built_customer_obj.customer_value	= $customer_address
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "System Install Address"
$as_built_customer_obj.customer_value	= $system_install_address	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Rma Address"
$as_built_customer_obj.customer_value	= $rma_address	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Rma Attn To Name"
$as_built_customer_obj.customer_value	= $rma_attn_to_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Domain Name"
$as_built_customer_obj.customer_value	= $domain_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Dns Server Ips"
$as_built_customer_obj.customer_value	= $dns_server_ips
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Ntp Server Names"
$as_built_customer_obj.customer_value	= $ntp_server_names	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Smtp Server Name"
$as_built_customer_obj.customer_value	= $smtp_server_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Snmp Server Name"
$as_built_customer_obj.customer_value	= $snmp_server_name	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Snmp Community String"
$as_built_customer_obj.customer_value	= $snmp_community_string
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Timezone"
$as_built_customer_obj.customer_value	= $timezone	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Default Admin User"
$as_built_customer_obj.customer_value	= $default_admin_user	
[Void]$as_built_customer.Add($as_built_customer_obj)
$as_built_customer_obj = "" | select customer_attribute, customer_value
$as_built_customer_obj.customer_attribute	= "Default Admin Pass"
$as_built_customer_obj.customer_value	= $default_admin_pass	
[Void]$as_built_customer.Add($as_built_customer_obj)

#endregion

# Build raidcom commands for all storage groups

$all_cmds = New-Object System.Collections.ArrayList

$raidcom_add_host_grp_cmds		= New-Object System.Collections.ArrayList
$raidcom_add_host_grp_brk_str 	= "Create Host Groups"
[Void]$raidcom_add_host_grp_cmds.Add((Build-CmdHeader $raidcom_add_host_grp_brk_str))

$raidcom_mod_host_grp_cmds		= New-Object System.Collections.ArrayList
$raidcom_mod_host_grp_brk_str 	= "Modify host groups"
[Void]$raidcom_mod_host_grp_cmds.Add((Build-CmdHeader $raidcom_mod_host_grp_brk_str))

$raidcom_add_hba_wwn_cmds		= New-Object System.Collections.ArrayList
$raidcom_add_hba_wwn_brk_str 	= "Add HBA WWNs"
[Void]$raidcom_add_hba_wwn_cmds.Add((Build-CmdHeader $raidcom_add_hba_wwn_brk_str))

$raidcom_set_hba_nickname_cmds	= New-Object System.Collections.ArrayList
$raidcom_set_hba_nickname_brk_str 	= "Set WWN Nicknames"
[Void]$raidcom_set_hba_nickname_cmds.Add((Build-CmdHeader $raidcom_set_hba_nickname_brk_str))

$raidcom_add_lun_map_cmds		= New-Object System.Collections.ArrayList
$raidcom_add_lun_map_brk_str 	= "Map LUNs to Host Groups"
[Void]$raidcom_add_lun_map_cmds.Add((Build-CmdHeader $raidcom_add_lun_map_brk_str))

# Setup host groups

foreach ($wwn_row in $wwns_data) 
	{
	$wwn_nickname		= $wwn_row.wwn_nickname
	$hba_wwn			= $wwn_row.hba_wwn	
	$tgt_port			= $wwn_row.tgt_port
	$host_grp_name		= $wwn_row.host_grp_name
	$host_grp_num		= $wwn_row.host_grp_num	
	$host_mode			= $wwn_row.host_mode
	$host_mode_opt		= $wwn_row.host_mode_opt

	if ($host_mode_opt -match ";")
		{
		$host_mode_opt_str = ($host_mode_opt).Replace(";", ",")
		}
	else
		{
		$host_mode_opt_str = $host_mode_opt
		}
	
	[Void]$raidcom_add_host_grp_cmds.Add("raidcom add host_grp -port $tgt_port -host_grp_name $host_grp_name")
	[Void]$raidcom_mod_host_grp_cmds.Add("raidcom modify host_grp -port $tgt_port $host_grp_name -host_mode $host_mode -host_mode_opt $host_mode_opt_str")
	[Void]$raidcom_add_hba_wwn_cmds.Add("raidcom add hba_wwn -port $tgt_port $host_grp_name -hba_wwn $hba_wwn")
	[Void]$raidcom_set_hba_nickname_cmds.Add("raidcom set hba_wwn -port $tgt_port $host_grp_name -hba_wwn $hba_wwn -wwn_nickname $wwn_nickname")
	}

# Create LDEVs, map LUNs to host groups

$host_grp_names_uniq = $ldevs_data | select host_grp_name -Unique

foreach ($host_grp_name in $host_grp_names_uniq)
	{
	$host_grp_name_str = $host_grp_name.host_grp_name
	
	# Get LDEV informtion for current storage group
	$ldev_rows 		= $ldevs_data | where {$_.host_grp_name -eq $host_grp_name_str}
	$host_grp_ports = $wwns_data | where {$_.host_grp_name -eq $host_grp_name_str} | select tgt_port

	foreach ($ldev_row in $ldev_rows)
		{
		# Build raidcom commands for each device in current storage group
		$ldev_id		= $ldev_row.ldev_id
		$pool_id		= $ldev_row.pool_id
		$capacity_gb	= $ldev_row.capacity_gb
		$host_grp_name	= $ldev_row.host_grp_name
		$lun_id			= $ldev_row.lun_id

		foreach ($tgt_port in $host_grp_ports)
			{
			$tgt_port_str = $tgt_port.tgt_port
			[Void]$raidcom_add_lun_map_cmds.Add("raidcom add lun -port $tgt_port_str $host_grp_name -ldev_id $ldev_id -lun_id $lun_id")
			}
		}
	}

[Void]$raidcom_add_host_grp_cmds.Add("")
[Void]$raidcom_mod_host_grp_cmds.Add("")
[Void]$raidcom_add_hba_wwn_cmds.Add("")
[Void]$raidcom_set_hba_nickname_cmds.Add("")
[Void]$raidcom_add_lun_map_cmds.Add("")

$all_cmds = $raidcom_add_host_grp_cmds + `
			$raidcom_mod_host_grp_cmds + `
			$raidcom_add_hba_wwn_cmds + `
			$raidcom_set_hba_nickname_cmds + `
			$raidcom_add_lun_map_cmds
			
if ($build_cmds -eq "y")
	{
	Write-Host ""
	Write-Host "$section_break"
	Write-Host "# + Writing svos_cmd_output_file:"
	Write-Host "$section_break"
	Write-Host ""
	Write-Host -ForegroundColor Green "$cmd_output_file"
	Write-Host ""
	$all_cmds | Out-File $cmd_output_file -Encoding ASCII
	}
