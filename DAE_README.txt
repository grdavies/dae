1.   Complete presite document with customer
1a.  Save your completed presite Word document in a SharePlan folder
1b.  Verify document synchronizes with SharePlan server (look for circle with checkmark)

2.  Login to DAE desktop 
2a.  https://view.datalink.com 
2b.  <<Add detailed steps for setting up View client here>>

3.  Retrieve presite document from SharePlan
3a. Double-click on "SharePlan" browser shortcut on desktop and login (https://dtlk-cp-01.datalink.com:4285/shareplan/#/login)
3b. Browse to presite Word document and choose Open
3c. Snap presite Word doc to right side of desktop screen

4.  Update global presite spreadsheet with presite data
4a. Double-click on desktop shortcut "global_presite_v2.4.xls - Shortcut"
4b. Snap presite Excel spreadsheet to left side of desktop screen
4c. Update the "global" Excel worksheets from left to right, beginning with "global_customers"

<<Need to denote all fields are required except those in color RED, these can be "not_used" if not applicable>>

global_customers 		=> customer_id	customer_name	contact_name	contact_email	contact_phone
global_locations 		=> location_id	customer_address	system_install_address	rma_address	rma_attn_to_name
global_datacenters		=> datacenter_id	datacenter_name	domain_name	dns_server_ips	ntp_server_names	smtp_server_name	snmp_server_name	snmp_community_string	timezone	default_admin_user	default_admin_pass
global_build_ids		=> build_id	customer_id	location_id	datacenter_id	cdot_cluster_name	cdot_node_model	esxi_cluster_name	esxi_host_model	esxi_src_iso
global_networks			=> build_id	network_label	vlan_id	hostname	ip_address	subnet	netmask	gateway

If building "esxi" module, update the following worksheets, otherwise skip:

esxi_adv				=> build_id	esxi_search_domains	esxi_boot_device	esxi_default_vm_net	esxi_enable_ssh	esxi_enable_shell	esxi_ssh_suppress_warn	esxi_shell_suppress_warn	esxi_local_vmfs_prefix
esxi_cluster_vswitches	=> build_id	esxi_vswitch_name	esxi_vswitch_num_ports	esxi_vswitch_mtu	esxi_vswitch_active_uplinks	esxi_vswitch_stdby_uplinks	cdp_status
esxi_hosts				=> build_id	esxi_hostname_fqdn	esxi_vmk0_ip	esxi_vmk0_nm	esxi_vmk0_gw	esxi_vmk1_ip	esxi_vmk1_nm	esxi_vmk2_ip	esxi_vmk2_nm	esxi_vmk3_ip	esxi_vmk3_nm	esxi_vmk4_ip	esxi_vmk4_nm	esxi_vmk5_ip	esxi_vmk5_nm
esxi_vmportgroups		=> build_id	esxi_pg_name	esxi_pg_vlan	esxi_pg_vswitch
esxi_vmkports			=> build_id	vmk_num	vswitch	vmk_role	vmk_pg_name	vmk_mtu	vmk_act_uplinks	vmk_stby_uplinks	vmk_vlan

If building "cdot" module, update the following worksheets, otherwise skip:

cdot_clusters			=> build_id	base_license	autosupport_enable	autosupport_transport	autosupport_email_to	ocum_user	ocpm_user
cdot_nodes				=> build_id	node_number	node_name	node_model	software_licenses
cdot_ifgrps				=> build_id	ten_gbe_ifgrp_mode	one_gbe_ifgrp_mode	ten_gbe_ifgrp_distr_func	one_gbe_ifgrp_distr_func
cdot_lifs				=> build_id	svm_number	network_label	vlan_id	node_number_range
cdot_aggrs				=> build_id	node_number	svm_number	action	flash_pool_enable	disk_size_str	disk_type	disk_count	raid_size	raid_type
cdot_svms				=> build_id	svm_number	ls_mir_sched_mins	ls_mir_sched_name

Note: Do not update the following "cdot" reference worksheets:
cdot_lif_attrs, cdot_controllers, cdot_disks, cdot_glossary

If building "svos" module, update the following worksheets, otherwise skip:

svos_wwns				=> build_id	wwn_nickname	hba_wwn	tgt_port	host_grp_name	host_grp_num	host_mode	host_mode_opt
svos_ldevs				=> build_id	ldev_id	pool_id	capacity_gb	host_grp_name	lun_id

5.  Execute build scripts
5a. Double-click "DAE Powershell" shortcut on desktop 
5b. Example esxi module build syntax (note: build_docs not current functional, specify n)
.\cc_builder_v2.5.ps1 -datasource_name "C:\tools\dae\global_presite_v2.5.xls" -build_cmds "y" -build_docs "n" -build_modules "esxi" -build_id 4

5c. Example esxi module build syntax
.\cc_builder_v2.5.ps1 -datasource_name "C:\tools\dae\global_presite_v2.5.xls" -build_cmds "y" -build_docs "y" -build_modules "cdot" -build_id 1

5d. Example svos module build syntax (note: build_docs not current functional, specify n)
.\cc_builder_v2.5.ps1 -datasource_name "C:\tools\dae\global_presite_v2.5.xls" -build_cmds "y" -build_docs "n" -build_modules "svos" -build_id 5

6.  Review script output
6a. Double-click "DAEOutput" shortcut on desktop
esxi => P:\<customer_name>\builds\build_id_<#>\esxi\esxi_<version>_<distro>_<customer_name>_build_<#>.iso

cdot => P:\<customer_name>\builds\build_id_<#>\cdot\<customer_name>_<cdot_cluster_name>_as-built.docx
cdot => P:\<customer_name>\builds\build_id_<#>\cdot\<customer_name>_<cdot_cluster_name>_cdot_build_cmds.txt
cdot => P:\<customer_name>\builds\build_id_<#>\cdot\<customer_name>_<cdot_cluster_name>_cdot_merged_data.csv

svos => P:\<customer_name>\builds\build_id_<#>\svos\<customer_name>_<svos_array_name>_svos_build_cmds.txt
